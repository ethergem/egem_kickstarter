/******************************************************************************/
//
//  EGEM Crowdfunding - campaigns/show.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import React, { Component } from 'react';
import { Button, Card, Container, Form, Grid, Header, Label, Message } from 'semantic-ui-react';
import Layout from '../../components/Layout';
import Campaign from '../../Egem/campaign';
import web3 from '../../Egem/web3';
import factory from '../../Egem/factory';
import ContributeForm from '../../components/ContributeForm';
import { Link, Router } from '../../routes';

class CampaignShow extends Component {

  state = {
    disableButtons: false,
    errorMessage: '',
    loadingReg: false,
    loadingViewRequests: false,
    loadingFinalized: false,
    loadingRefund: false
  };

  static async getInitialProps(props) {
    const accounts = await web3.eth.getAccounts();
    const campaign = Campaign(props.query.address);
    // Get Campaign contract data
    const summary = await campaign.methods.getSummary().call();
    const isApprover =  await campaign.methods.approvers(accounts[0]).call();
    const campaignOwner = await campaign.methods.manager().call();
    const campaignRefunded = await campaign.methods.refundedCampaign().call();
    const isContributor = await campaign.methods.inLottery(accounts[0]).call();
    // Get CampaignFactory contract data
    const devNickname = await factory.methods.devNickname(summary[4]).call();
    const isCoreDev = await factory.methods.isCoreDev(accounts[0]).call();
    const indexCampaign = await factory.methods.indexCampaign(props.query.address).call();
    const infoCampaigns = await factory.methods.infoCampaigns(indexCampaign).call();
    const campaignFinalized = await factory.methods.campaignFinalized(props.query.address).call();

    const urlProject = infoCampaigns.urlProject;
    var summaryInfo = infoCampaigns.summaryInfo;
    var campaignState = "Active";
    var isOwner = false;

    if (accounts[0] == campaignOwner) isOwner = true;
    if (campaignRefunded) {
      campaignState = "Refund";
      summaryInfo += " (REFUND)";
    }
    if (campaignFinalized) {
      campaignState = "Finalized";
      summaryInfo += " (FINALIZED)";
    }

    return {
      address: props.query.address,
      minimumContribution:  summary[0],
      balance:              summary[1],
      requestsCount:        summary[2],
      approversCount:       summary[3],
      manager:              summary[4],
      lotteryPercentage:    summary[5],
      devNickname:          devNickname,
      isCoreDev:            isCoreDev,
      isApprover:           isApprover,
      isOwner:              isOwner,
      isContributor:        isContributor,
      summaryInfo:          summaryInfo,
      urlProject:           urlProject,
      campaignState:        campaignState
    };
  }

  onRegister = async (event) => {
    event.preventDefault();

    this.setState({ disableButtons: true, loadingReg: true, errorMessage: '' });
    try {
      const accounts = await web3.eth.getAccounts();
      const campaign = Campaign(this.props.address);
      await campaign.methods.campaignApprover().send({
        from: accounts[0]
      });
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ loadingReg: false });
    Router.replaceRoute(`/campaigns/${this.props.address}`);
  };

  onFinalized = async (event) => {
    event.preventDefault();

    this.setState({ disableButtons: true, loadingFinalized: true, errorMessage: '' });
    try {
      const accounts = await web3.eth.getAccounts();
      await factory.methods.finalizeCampaign(this.props.address).send({
        from: accounts[0]
      });
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ disableButtons: false, loadingFinalized: false });
    Router.replaceRoute(`/campaigns/${this.props.address}`);
  };

  onVoteFinalized = async (event) => {
    event.preventDefault();

    this.setState({ disableButtons: true, loadingFinalized: true, errorMessage: '' });
    try {
      const accounts = await web3.eth.getAccounts();
      await factory.methods.voteFinalizeCampaign(this.props.address).send({
        from: accounts[0]
      });
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ disableButtons: false, loadingFinalized: false });
    Router.replaceRoute(`/campaigns/${this.props.address}`);
  };

  onVoteRefund = async (event) => {
    event.preventDefault();

    this.setState({ disableButtons: true, loadingRefund: true, errorMessage: '' });
    try {
      const accounts = await web3.eth.getAccounts();
      const campaign = Campaign(this.props.address);
      await campaign.methods.refundCampaign().send({
        from: accounts[0]
      });
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ disableButtons: false, loadingRefund: false });
    Router.replaceRoute(`/campaigns/${this.props.address}`);
  };

  renderCards() {
    const {
      balance,
      manager,
      lotteryPercentage,
      minimumContribution,
      requestsCount,
      approversCount
    } = this.props;

    // Get campaign balance in Ether
    var balanceEth = web3.utils.fromWei(balance,'ether');
    // Calculate balances available for Developer and for Lottery pool
    const balanceLottery = parseFloat(balanceEth) * parseFloat(lotteryPercentage) / 100.0;
    const balanceDev = parseFloat(balanceEth) * (1.0 - parseFloat(lotteryPercentage) / 100.0);

    const items = [
      {
        header: this.props.devNickname,
        meta: 'Developer Nickname',
        description: 'This developer created this campaign. He can create requests to withdraw funds.',
        style: { overflowWrap: 'break-word' }
      },
      {
        header: web3.utils.fromWei(minimumContribution,'ether'),
        meta: 'Minimum Contribution (EGEM)',
        description: 'You must contribute at least this much Wei to support this project. As a contributor you get included in the campaign Lottery contract.',
        style: { overflowWrap: 'break-word' }
      },
      {
        header: requestsCount,
        meta: 'Number of Requests',
        description: 'A request tries to withdraw money from the contract. Only the project developer can submit a request and this must be approved by EGEM Core developer.',
        style: { overflowWrap: 'break-word' }
      },
      {
        header: approversCount,
        meta: 'Number of Supporters',
        description: 'Number of people that have already donated to this campaign.',
        style: { overflowWrap: 'break-word' }
      },
      {
        header: web3.utils.fromWei(balance,'ether'),
        meta: 'Campaign Balance (EGEM)',
        description: 'This balance is the total funds available in this Campaign. This includes Developer+Lottery funds.',
        style: { overflowWrap: 'break-word' }
      },

      {
        header: parseFloat(balanceDev).toFixed(3),
        meta: 'Availabel Developer Balance (EGEM)',
        description: 'This is the maximum balance available for developer withdrawals.',
        style: { overflowWrap: 'break-word' }
      },
      {
        header: parseFloat(balanceLottery).toFixed(3),
        meta: `${this.props.lotteryPercentage}% Lottery Balance (EGEM)`,
        description: 'The Lottery balance is how much money this campaign has left for the Lottery pool.',
        style: { overflowWrap: 'break-word' }
      },
      {
        header: `${this.props.campaignState}`,
        meta: 'Current Campaign State',
        description: 'The campaign can be in Active, Finalized or Refund state.',
        style: { overflowWrap: 'break-word' }
      }
    ];

    return <Card.Group items={items} />;
  }

  render() {
    return(
      <Layout>
        <Container text>
          <Header as='h2'
            textAlign='center'
            style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}>
            Project Details
          </Header>
        </Container>

        <Grid column={2} divided>
          <Grid.Row>
            <Grid.Column>
              <Card.Group style={{color: 'black'}}>
                <Card fluid header='Project Title' description={this.props.summaryInfo} />
              </Card.Group>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <Card.Group style={{color: 'black'}}>
                <Card
                  fluid
                  header='Project URL (optional)'
                  description={<a href={`${this.props.urlProject}`} target="_blank">{this.props.urlProject}</a>}
                  />
              </Card.Group>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column width={12}>
              {this.renderCards()}
            </Grid.Column>
            <Grid.Column width={3}>
              {!this.props.isOwner ?
                <ContributeForm
                  disableButtons={this.state.disableButtons || this.props.campaignState != "Active"}
                  address={this.props.address}
                  isContributor={this.props.isContributor}
                  campaignState={this.props.campaignState}/>
              :
              <Form
                onSubmit={this.onFinalized}
                error={!!this.state.errorMessage}
                style={{ marginTop: '20px'}}>
                <Label>You are the Campaign Owner</Label>
                <Label
                  style={{ marginTop: '20px'}}>
                  If campaign goals have been achieved and balance is 0 EGEM, then you can Finilize the campaign.
                </Label>
                <Button
                  disabled={this.state.disableButtons || this.props.campaignState == "Finalized" || this.props.balance != 0}
                  loading={this.state.loadingFinalized}
                  primary>
                  Finalize
                </Button>
              </Form>
              }

              <Form
                onSubmit={this.onVoteFinalize}
                error={!!this.state.errorMessage}
                style={{ marginTop: '20px'}}>
                <Message error header="Oops!" content={this.state.errorMessage} />
                {this.props.isCoreDev && this.props.isApprover ?
                  <div>
                  <Label>You are an Approver.</Label>
                  <Label style={{ marginTop: '20px'}}>If campaign goals have been achieved and balance is 0 EGEM, then you can vote to Finilize the campaign.</Label>
                  <Button
                    disabled={this.state.disableButtons || this.props.campaignState != "Active" || this.props.balance != 0}
                    loading={this.state.loadingFinalized}
                    primary>
                    Vote to Finalize
                  </Button>
                  </div>
                  : null}
              </Form>

              <Form
                onSubmit={this.onVoteRefund}
                error={!!this.state.errorMessage}
                style={{ marginTop: '20px'}}>
                {this.props.isCoreDev && this.props.isApprover ?
                  <div>
                  <Label style={{ marginTop: '20px'}}>If campaign needs to be Refunded, then you can vote to Refund the campaign.</Label>
                  <Button
                    disabled={this.state.disableButtons || this.props.campaignState != "Active"}
                    loading={this.state.loadingRefund}
                    primary>
                    Vote to Refund
                  </Button>
                  </div>
                  : null}
              </Form>

              <Form
                onSubmit={this.onRegister}
                error={!!this.state.errorMessage}
                style={{ marginTop: '20px'}}>
                {this.props.isCoreDev && !this.props.isApprover ?
                  <div>
                  <Label>You are a Core Developer Register as an Approver</Label>
                  <Button
                    disabled={this.state.disableButtons || this.props.campaignState != "Active"}
                    loading={this.state.loadingReg}
                    primary>
                    Register
                  </Button>
                  </div>
                : null}
              </Form>

            </Grid.Column>
          </Grid.Row>

          <Grid.Row>
            <Grid.Column>
              <Link route={`/campaigns/${this.props.address}/requests`}>
                <a>
                  <Button
                    disabled={this.state.disableButtons}
                    loading={this.state.loadingViewRequests}
                    onClick={event => this.setState({ disableButtons: true, loadingViewRequests: true })}
                    primary>
                    View Requests
                  </Button>
                </a>
              </Link>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Layout>
    );
  }
}

export default CampaignShow;
