/******************************************************************************/
//
//  EGEM Crowdfunding - campaigns/new.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import React, { Component } from 'react';
import { Button, Container, Dropdown, Form, Header, Input, Label, Message, Table } from 'semantic-ui-react';
import Layout from '../../components/Layout';
import factory from '../../Egem/factory';
import web3 from '../../Egem/web3';
import { Router } from '../../routes';

class CampaignNew extends Component {
  state = {
    projectSummary: '',
    urlProject: '',
    minimumContribution: '',
    errorMessage: '',
    loadingPreview: false,
    loadingSent: false,
    lotteryPercentage: '',
    errorPercentage: false
  };

  static async getInitialProps() {
    const accounts = await web3.eth.getAccounts();
    const devNickname = await factory.methods.devNickname(accounts[0]).call();
    const devData = await factory.methods.getDevData(accounts[0]).call();
    const contractData = await factory.methods.getContractData().call();

    return {
      devNickname: devNickname,
      activeCampaigns: devData[6],
      maxCampaigns: contractData[5]
     };
  }

  onClear = (event) => {
    event.preventDefault();
    this.setState({
      devNickname: '',
      projectSummary: '',
      urlProject: '',
      minimumContribution: '',
      errorMessage: '',
      loadingPreview: false,
      loadingSent: false,
      lotteryPercentage: ''
    });
  }

  onPreview = (event) => {
    event.preventDefault();
    if (this.state.lotteryPercentage >= 0 && this.state.lotteryPercentage <= 100) {
      this.setState({ lotteryPercentage:  Math.round(this.state.lotteryPercentage) });
      this.setState({ loadingPreview: true, errorMessage: '' });
    } else {
      this.setState({ errorMessage: 'Lottery percentage must be a valid integer between 0 and 100' });
    }
  }

  onSubmit = async (event) => {
    event.preventDefault();

    // Check if the minimumContribution and projectSummary have been set by developer
    // if not, then do not send request to smart contract
    if (this.state.minimumContribution != '' && this.state.projectSummary != '')
    {
      this.setState({ loadingSent: true, errorMessage: '' });
      try {
        const accounts = await web3.eth.getAccounts();
        await factory.methods
          .createCampaign(
            web3.utils.toWei(this.state.minimumContribution,'ether'),
            this.state.projectSummary,
            this.props.devNickname,
            this.state.urlProject,
            this.state.lotteryPercentage
          ).send({
            from: accounts[0]
          });

        // Redirect user to the Campaign index page
        Router.pushRoute('/');
      } catch (err) {
        this.setState({ errorMessage: err.message });
      }
    }
    this.setState({ loadingPreview: false, loadingSent: false });
  };

  render () {
    const { devNickName } = this.props;

    return (
      <Layout>
          {this.state.loadingPreview ? null: (
            <Container>
              <Header
                as='h2'
                textAlign='center'
                style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}>
                Create a New EGEM Crowdfunding Campaign
              </Header>
              <Form onSubmit={this.onPreview} error={!!this.state.errorMessage} >
                <Form.Group>
                  <Label size={'medium'}>
                    Your Developer Nickname: {`${this.props.devNickname}`}
                  </Label>
                  <Label size={'medium'}>
                    You have {`${this.props.activeCampaigns}`} active Campaigns (Max. {`${this.props.maxCampaigns}`}).
                  </Label>
                </Form.Group>

                {this.props.activeCampaigns == this.props.maxCampaigns ?
                  <Message
                    header='Reached Max. Number of Active Campaigns'
                    content={
                      <div className="ui justified container">
                      <p>
                      You have reached the maximum number of allowed active campaigns. If you would like to create a
                      new campaign, you will need to finalize one, or more, of your current active campaigns.
                      </p>
                      </div>
                    }
                  />
                :
                <div>
                <Form.Group>
                  <Label size={'medium'}>Project Summary</Label>
                  <Form.Input
                    fluid placeholder='Provide a brief project summary'
                    value={this.state.projectSummary}
                    onChange={event => this.setState({ projectSummary: event.target.value })}
                    width={16}>
                  </Form.Input>
                </Form.Group>

                <Form.Group>
                  <Label size={'medium'}>Project URL (Optional)</Label>
                  <Form.Input
                    fluid placeholder='Optional Project URL'
                    value={this.state.urlProject}
                    onChange={event => this.setState({ urlProject: event.target.value })}
                    width={14}>
                  </Form.Input>
                </Form.Group>

                <Form.Group>
                  <Label size={'medium'}>Minimum Contribution (EGEM)</Label>
                  <Form.Input
                    fluid placeholder='Provide a minimum contribution'
                    value={this.state.minimumContribution}
                    onChange={event => this.setState({ minimumContribution: event.target.value })}
                    width={4}>
                  </Form.Input>
                </Form.Group>

                <Form.Group>
                  <Label size={'medium'}>Lottery Percentage:</Label>
                  <Form.Input
                    fluid placeholder='Lottery percentage (0-100)'
                    value={this.state.lotteryPercentage}
                    onChange={event => this.setState({ lotteryPercentage: event.target.value })}
                    width={4}>
                  </Form.Input>
                </Form.Group>

                <Message error header="Oops!" content={this.state.errorMessage}>
                </Message>
                <Button
                  disabled={this.state.loadingSent}
                  floated='right'
                  loading={this.state.loadingSent}
                  primary>
                    Preview
                </Button>
                </div> }
              </Form>
            </Container>
          )}

        {!this.state.loadingPreview ? null: (
          <Container>
            <Header
              as='h2'
              textAlign='center'
              style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}>
              Preview Project Details
            </Header>

            <Message
              header='Important'
              content={
                <div className="ui justified container">
                <p>
                You are about to submit a new project request for funding. The information provided in this
                form will be stored in the EGEM network blockchain. Please make sure the information you have
                provided is accurate before creating the project.
                </p>
                <p>
                If you provided an URL for your project, please click and make sure a new window redirects you
                to the desired project site.
                </p>
                </div>
              }
            />

            <Table celled style={{ color: 'black', marginTop: 10, marginBottom: 10 }}>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell width={3}>
                    <Label>Dev. Nickname</Label>
                  </Table.HeaderCell>
                  <Table.HeaderCell width={10}>
                    <Label>Project Summary</Label>
                  </Table.HeaderCell>
                  <Table.HeaderCell width={3}>
                    <Label>Minimum Contribution (EGEM)</Label>
                  </Table.HeaderCell>
                </Table.Row>
              </Table.Header>

              <Table.Body>
                <Table.Row>
                  <Table.Cell>
                    <p>{this.props.devNickname}</p>
                  </Table.Cell>
                  <Table.Cell>
                    <p>{this.state.projectSummary}</p>
                  </Table.Cell>
                  <Table.Cell>
                    <p>{this.state.minimumContribution}</p>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <Label style={{ marginRight: 10 }}>Project URL</Label>
                    <a href={this.state.urlProject} target="_blank">
                    {this.state.urlProject}
                    </a>
                  </Table.Cell>
                </Table.Row>
                <Table.Row>
                  <Table.Cell>
                    <Label style={{ marginRight: 10 }}>
                      Lottery Percentage: {this.state.lotteryPercentage} %
                    </Label>
                  </Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>

            <Button.Group floated='right'>
              <Button
                disabled={this.state.loadingSent}
                loading={this.state.loadingSent}
                onClick={this.onSubmit}
                primary>
                Send
              </Button>
              <Button
                disabled={this.state.loadingSent}
                onClick={this.onClear}
                secondary>
                Clear
              </Button>
            </Button.Group>
          </Container>
        )}
      </Layout>
    );
  }
}

export default CampaignNew;
