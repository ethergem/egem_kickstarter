/******************************************************************************/
//
//  EGEM Crowdfunding - requests/new.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import React, { Component } from 'react';
import { Button, Container, Form, Header, Input, Label, Message, Table } from 'semantic-ui-react';
import Campaign from '../../../Egem/campaign';
import web3 from '../../../Egem/web3';
import { Link, Router } from '../../../routes';
import Layout from '../../../components/Layout';


class RequestNew extends Component {
  state = {
    value: '',
    description: '',
    recipient: '',
    errorMessage: '',
    errorBalance: '',
    loadingCreate: false,
    loadingBack: false,
    loadingPreview: false
  };

  constructor(props) {
    super(props);
    this.checkAccount();
  }

  async checkAccount() {
    const accounts = await web3.eth.getAccounts();
    if (accounts.length != 0) {
      this.setState({recipient: accounts[0]});
    }
  }

  static async getInitialProps(props) {
    const addressCampaign = props.query.address;
    const campaign = Campaign(addressCampaign);
    const requestCount = await campaign.methods.getRequestsCount().call();
    const summary = await campaign.methods.getSummary().call();
    const balanceCampaign = summary[1];
    var balanceRequests = 0.0;

    const requests = await Promise.all(
      Array(parseInt(requestCount)).fill().map((element, index) => {
        return campaign.methods.requests(index).call();
      })
    );

    requests.map((request, index) => {
      if (!request.complete) {
        // Only add balance of pending requests
        balanceRequests += parseFloat(web3.utils.fromWei(request.value,'ether'));
      }
    });

    return { addressCampaign, balanceCampaign, requests, balanceRequests };
  }

  onClear = (event) => {
    event.preventDefault();
    this.setState({
      value: '',
      description: '',
      errorMessage: '',
      loadingPreview: false
    });
  }

  onPreview = (event) => {
    event.preventDefault();

    this.setState({ errorBalance: ''});
    // Verify that requested withdrawal value does not exceed current
    // campaign balance
    const leftBalance = this.props.balanceCampaign - this.props.balanceRequests;
    if (this.state.value > leftBalance) {
      this.setState({ errorBalance: "Requested value exceeds remainging campaign balance"});
    } else {
      this.setState({ loadingPreview: true });
    }
  }

  onSubmit = async (event) => {
    event.preventDefault();

    // verify that requested value is less or equal than current
    // campaign balance.
    if (this.state.value > this.props.balanceCampaign) {
      this.setState({ errorMessage: "Requested funds is greater than current balance" });
    } else {
      const campaign = Campaign(this.props.addressCampaign);
      const { description, value, recipient } = this.state;

      this.setState({ loadingCreate: true, errorMessage: '' });
      try {
        const accounts = await web3.eth.getAccounts();

        await campaign.methods.createRequest(
          description,
          web3.utils.toWei(value,'ether'),
          recipient
        ).send({ from: accounts[0] });

        //Router.pushRoute(`/campaigns/${this.props.address}/requests`)
      } catch (err) {
        this.setState({ errorMessage: err.message });
      }
      this.setState({ loadingCreate: false });
      Router.pushRoute(`/campaigns/${this.props.addressCampaign}/requests`);
    }
  };

  render() {
    return (
      <Layout>
        { !this.state.loadingPreview ?
          <div>
          <Header
            as='h2'
            style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}
            textAlign='center'>
            Create a Withdrawal Request
          </Header>
          <Form onSubmit={this.onPreview}>
            <Form.Field>
              <Container
                style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}>
                  <label>Provide a Description</label>
              </Container>
              <Input
                value={this.state.description}
                onChange={event => this.setState({ description: event.target.value })}
              />
            </Form.Field>

            <Form.Field>
              <Container
                style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}>
                  <label>Withdrawal Value in Ether</label>
              </Container>
              <Input
                value={this.state.value}
                onChange={event => this.setState({ value: event.target.value })}
              />
            </Form.Field>

            <Form.Field>
              <Message
                style={{marginTop: '20px', marginBottom: '20px'}}>
                <Message.Header>Developer EGEM Address (Recipient):</Message.Header>
                <p>{this.state.recipient}</p>
              </Message>
            </Form.Field>

            <Form.Field>
              <Message
                style={{marginTop: '20px', marginBottom: '20px'}}>
                <Message.Header>Current Campaign Balance:</Message.Header>
                <p>{web3.utils.fromWei(this.props.balanceCampaign,'ether')}</p>
              </Message>
            </Form.Field>

            <Button
              disabled={this.state.loadingPreview}
              loading={this.state.loadingPreview}
              primary>
              Preview
            </Button>
            <Link route={`/campaigns/${this.props.addressCampaign}/requests`}>
              <a>
                <Button
                  disabled={this.state.loadingPreview}
                  loading={this.state.loadingBack}
                  onClick={event => this.setState({ loadingBack: true })}
                  primary>
                  Back
                </Button>
              </a>
            </Link>
          </Form>
          </div>
        :
          <div>
          <Header
            as='h2'
            style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}
            textAlign='center'>
            Preview Withdrawal Request Details
          </Header>
          <Message
            header='Important'
            content={
              <div className="ui justified container">
              <p>
              You are about to submit a new withdrawal request. The information provided in this
              form will be stored in the EGEM network blockchain. Make sure the information you have
              provided is accurate before registering this request.
              </p>
              </div>
            }
          />
          <Table celled style={{ color: 'black', marginTop: 10, marginBottom: 10 }}>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell width={6}>
                  <Label>Recipient Address</Label>
                </Table.HeaderCell>
                <Table.HeaderCell width={8}>
                  <Label>Description</Label>
                </Table.HeaderCell>
                <Table.HeaderCell width={2}>
                  <Label>Withdrawal Amount (EGEM)</Label>
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>

            <Table.Body>
              <Table.Row>
                <Table.Cell>
                  <p>{this.state.recipient}</p>
                </Table.Cell>
                <Table.Cell>
                  <p>{this.state.description}</p>
                </Table.Cell>
                <Table.Cell>
                  <p>{this.state.value}</p>
                </Table.Cell>
              </Table.Row>
            </Table.Body>
          </Table>

          {this.state.errorMessage ?
            <Message error header="Oops!" content={this.state.errorMessage} />
            : null }

          <Button.Group floated='right'>
            <Button
              disabled={this.state.loadingCreate}
              loading={this.state.loadingCreate}
              onClick={this.onSubmit}
              primary>
              Create!
            </Button>
            <Button
              disabled={this.state.loadingCreate}
              onClick={this.onClear}
              secondary>
              Clear
            </Button>
          </Button.Group>
          </div>
        }

        { this.state.errorBalance ?
          <Message
            error
            style={{marginTop: '20px', marginBottom: '20px'}}
            header="Oops!!!"
            content={this.state.errorBalance}
            >
          </Message> : null }

      </Layout>
    );
  }
}

export default RequestNew;
