/******************************************************************************/
//
//  EGEM Crowdfunding - registerdev.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import React, { Component } from 'react';
import { Button, Container, Dropdown, Form, Header, Input, Label, Message, Table } from 'semantic-ui-react';
import Layout from '../components/Layout';
import factory from '../Egem/factory';
import web3 from '../Egem/web3';
import { Router } from '../routes';

class RegisterDev extends Component {
  state = {
    devNickname: '',
    coreDevMode: false,
    errorMessage: '',
    loadingPreview: false,
    loadingSent: false
  };

  static async getInitialProps(props) {
    return {
      address: props.query.address
    };
  }

  onClear = (event) => {
    event.preventDefault();
    this.setState({
      devNickname: '',
      coreDevMode: false,
      errorMessage: '',
      loadingPreview: false,
      loadingSent: false
    });
  }

  onPreview = (event) => {
    event.preventDefault();
    this.setState({ loadingPreview: true, errorMessage: '' });
  }

  selectDevMode = (event, {value}) => {
    event.preventDefault();
    if (value == 2) {
      this.setState({ coreDevMode: true});
      console.log("CORE DEVELOPER");
    }
    else {
      this.setState({ coreDevMode: false});
      console.log("COMMUNITY DEVELOPER");
    }
  }

  onSubmit = async (event) => {
    event.preventDefault();

    this.setState({ loadingSent: true, errorMessage: '' });
    try {
      const accounts = await web3.eth.getAccounts();
      await factory.methods
        .registerDev(this.state.devNickname, this.state.coreDevMode)
        .send({
          from: accounts[0]
        });

      // Redirect user to the Campaign index page
      Router.pushRoute('/');
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ loadingPreview: false, loadingSent: false });
  };

  render () {
    return (
      <Layout>
          {this.state.loadingPreview ? null: (
            <Container>
              <Header
                as='h2'
                style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}
                textAlign='center'>
                Register as a New Developer
              </Header>
              <Form onSubmit={this.onPreview} error={!!this.state.errorMessage}>
                <Form.Group>
                  <Label size={'medium'}>Dev Nickname</Label>
                  <Form.Input
                    fluid placeholder='Provide your Dev Nickname'
                    value={this.state.devNickname}
                    onChange={event => this.setState({ devNickname: event.target.value })}
                    width={4}>
                  </Form.Input>
                  <Label size={'medium'} style={{marginRight: '10px'}}> Account Address</Label>
                  {this.props.address}
                </Form.Group>

                <Dropdown
                  defaultValue={1}
                  onChange={this.selectDevMode}
                  options={[
                    { key: 1, text: 'Register as Community Developer', value: 1 },
                    { key: 2, text: 'Register as Core Developer', value: 2 },
                  ]} simple item />

                <Message error header="Oops!" content={this.state.errorMessage}>
                </Message>
                <Button
                  disabled={this.state.loadingSent}
                  floated='right'
                  loading={this.state.loadingSent}
                  primary>
                    Preview
                </Button>

              </Form>
            </Container>
          )}

        {!this.state.loadingPreview ? null: (
          <Container>
            <Header
              as='h2'
              style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}
              textAlign='center'>
              Preview Registration Details
            </Header>

            <Message
              header='Important'
              content={
                <div className="ui justified container">
                <p>
                You are about to submit a new developer request for registration. The information provided in this
                form will be stored in the EGEM network blockchain. Make sure the information you have
                provided is accurate before registering as a developer.
                </p>
                <p>
                If you are applying to become a Community Developer, then your request will need to be approved by
                any other active Core or Community developer. On the other hand, if you are applying to become a Core
                developer, then your account will require approval from an active Core developer.
                </p>
                </div>
              }
            />

            <Table celled style={{ color: 'black', marginTop: 10, marginBottom: 10 }}>
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell width={3}>
                    <Label>Dev. Nickname</Label>
                  </Table.HeaderCell>
                  <Table.HeaderCell width={10}>
                    <Label>Address</Label>
                  </Table.HeaderCell>
                  <Table.HeaderCell width={10}>
                    <Label>Status</Label>
                  </Table.HeaderCell>
                </Table.Row>
              </Table.Header>

              <Table.Body>
                <Table.Row>
                  <Table.Cell>
                    <p>{this.state.devNickname}</p>
                  </Table.Cell>
                  <Table.Cell>
                    <p>{this.props.address}</p>
                  </Table.Cell>
                  <Table.Cell>
                    {this.state.coreDevMode ?
                      <Label>CORE DEVELOPER</Label> :
                      <Label>COMMUNITY DEVELOPER</Label>}
                  </Table.Cell>
                </Table.Row>
              </Table.Body>
            </Table>

            <Button.Group floated='right'>
              <Button
                disabled={this.state.loadingSent}
                loading={this.state.loadingSent}
                onClick={this.onSubmit}
                primary>
                Send
              </Button>
              <Button
                disabled={this.state.loadingSent}
                onClick={this.onClear}
                secondary>
                Clear
              </Button>
            </Button.Group>
          </Container>
        )}
      </Layout>
    );
  }
}

export default RegisterDev;
