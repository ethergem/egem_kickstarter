/******************************************************************************/
//
//  EGEM Crowdfunding - index.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import React, { Component } from 'react';
import { Button, Checkbox, Container, Dropdown, Form, Header, Label } from 'semantic-ui-react';
import factory from '../Egem/factory';
import Layout from '../components/Layout';
import RenderDevelopers from '../components/RenderDevelopers'
import RequestCoreDevRow from '../components/RequestCoreDevRow';
import RequestCommDevRow from '../components/RequestCommDevRow';
import { Link, Router } from '../routes';
import web3 from '../Egem/web3';

class CampaignIndex extends Component {

  state = {
    disableButtons: false,
    addressMetamask: '',
    managerAddress: '',
    campaignRoute: '',
    campaignAddress: '',
    createCampaign: false,
    devRegister: false,
    loadCampaign: false,
    showCoreDevs: false,
    showCommDevs: false,
    loadingViewDetails: false,
    statusAccounts: false,
    isManager: false,
    isCoreDev: false,
    isCommDev: false,
    showFinalized: false
  };

  constructor(props) {
    super(props);
    this.checkAccount();
  }

  async checkAccount() {
    const accounts = await web3.eth.getAccounts();

    if (accounts.length != 0) {
      // Store current Metamask active Address
      this.setState({ addressMetamask: accounts[0] });
      // Get the CampaignFactory manager address.
      const managerAddress = await factory.methods.manager().call();
      this.setState({ managerAddress: managerAddress });
      // Verify if account is contract Manager.
      if (accounts[0] == managerAddress) {
          this.setState({ isManager: true });
      }
      const devData = await factory.methods.getDevData(accounts[0]).call();
      // Verify if account is registered as Core Developer.
      const isCoreDev = devData[0];
      if (isCoreDev) {
          this.setState({ showCoreDevs: false, showCommDevs: false, isCoreDev: true });
      }
      // Verify if account is registered as Community Developer.
      const isCommDev = devData[1];
      if (isCommDev) {
          this.setState({ showCommDevs: false, isCommDev: true });
      }
    }
  }

  static async getInitialProps() {
    const campaigns = await factory.methods.getDeployedCampaigns().call();
    const campaignsCount = await factory.methods.getCampaignsCount().call();
    const contractData = await factory.methods.getContractData().call();
    const coreDevCount = contractData[3];
    const commDevCount = contractData[4];


    const infoCoreDevs = await Promise.all(
      Array(parseInt(coreDevCount)).fill().map((element, index) => {
        return factory.methods.infoCoreDevs(index).call();
      })
    );

    const infoCommDevs = await Promise.all(
      Array(parseInt(commDevCount)).fill().map((element, index) => {
        return factory.methods.infoCommDevs(index).call();
      })
    );

    const infoCampaigns = await Promise.all(
      Array(parseInt(campaignsCount)).fill().map((element, index) => {
        return factory.methods.infoCampaigns(index).call();
      })
    );

    return { campaigns, infoCoreDevs, infoCommDevs, infoCampaigns, campaignsCount };
  }

  renderCampaigns() {
    var newIndex = 0;
    var prevIndex = [];
    var filteredCampaigns = [];
    for (var i = 0; i < this.props.campaignsCount; i++) {
      if (!this.props.infoCampaigns[i].isFinalized ||
          (this.props.infoCampaigns[i].isFinalized && this.state.showFinalized)) {
        filteredCampaigns[newIndex] = this.props.campaigns[i];
        prevIndex[newIndex] = i;
        newIndex++;
      }
    }
    const items = filteredCampaigns.map((address,index) => {
      return {
        key: index,
        text: `
          Developer (${this.props.infoCampaigns[prevIndex[index]].ownerNickname}) -
          ${this.props.infoCampaigns[prevIndex[index]].summaryInfo}
          ${this.props.infoCampaigns[prevIndex[index]].isFinalized ? " (FINALIZED)": ""}`,
        value: address
      };
    });
    return items;
  }

  onCreateCampaign = (event) => {
    event.preventDefault();
    this.setState({ disableButtons: true, createCampaign: true });
    // Redirect user to the create Campaign page
    Router.pushRoute("/campaigns/new");
  }

  onDevRegister = (event) => {
    event.preventDefault();
    this.setState({ disableButtons: true, devRegister: true });
    // Redirect user to the create Campaign page
    Router.pushRoute(`/${this.state.addressMetamask}/registerdev`);
  }

  onCampaignInfo = (event, {value}) => {
    event.preventDefault();
    // value - value holds the address to the selected Dropdown campaign
    let address = event.target.textContent;
    this.setState({ campaignRoute: `/campaigns/${value}`, campaignAddress: `${value}`, loadCampaign: true });
  }

  render() {
    return (
      <Layout>
        <Header
          as='h2'
          style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}
          textAlign='center'>
          EGEM Crowdfunding Projects
        </Header>

        {(this.state.isCoreDev || this.state.isCommDev) ? null :
          <a>
          <Button
            floated='right'
            content='Dev Register'
            icon='add circle'
            disabled={this.state.disableButtons}
            loading={this.state.devRegister}
            onClick={this.onDevRegister}
            primary
          />
          </a>
        }

        {(this.state.isCoreDev || this.state.isCommDev) ?
          <a>
          <Button
            floated='right'
            content='Create Project'
            icon='add circle'
            disabled={this.state.disableButtons}
            loading={this.state.createCampaign}
            onClick={this.onCreateCampaign}
            primary
          />
          </a>
        : null
        }

        <Header
          as='h3'
          style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}
          textAlign='left'>
          Support EGEM Development - BETA Test Version (EtherGem Network)
        </Header>
        <p>
        The EGEM Crowdfunding Project is deployed on an EtherGem smart contract and  provides a way for the community
        to support EtherGem development initiatives. You may request to join as an active developer, or
        alternatively  you can help fund projects under development.
        </p>
        <p>
        To support current active development projects, you can select one of them using the Drop-Down menu and then
        click on the "View" button to get further project details.
        </p>

        {!this.state.isCoreDev && !this.state.isCommDev ?
          <p>
          To submit a request to become an EtherGem developer, click on the "Dev Register" button and submit your request.
          </p>
        : <div>
          <Header
            as='h3'
            style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}
            textAlign='left'>
            Welcome Developer
          </Header>

          <p>
          You can create a new EtherGem development project by clicking on the "Create Project" button.
          </p>
          </div>
        }

        <Header
          as='h3'
          style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}
          textAlign='center'>
          View Active Projects
        </Header>

        <Form>
          <Checkbox onChange={event => this.setState({ showFinalized: !this.state.showFinalized })}/>
          <Label>Show Finalized</Label>
        </Form>

        <Dropdown
          placeholder='Active Projects'
          fluid
          search
          selection
          style={{marginTop: '20px'}}
          options={this.renderCampaigns()}
          onChange={this.onCampaignInfo}
          width={14}
        />

        {!this.state.loadCampaign ? null: (
          <Container>
            <Link route={this.state.campaignRoute}>
              <a>
              <Button.Group>
              <Button
                content='View Details'
                primary
                style={{marginTop: '20px'}}
                disabled={this.state.disableButtons}
                loading={this.state.loadingViewDetails}
                onClick={event => this.setState({ disableButtons: true, loadingViewDetails: true })}
              />
              </Button.Group>
              </a>
            </Link>
          </Container>
        )}

        {this.state.isCoreDev || this.state.isCommDev ?
          <div>
          <Button
            disabled={this.state.disableButtons}
            content='View Core Devs'
            primary={this.state.showCoreDevs}
            secondary={!this.state.showCoreDevs}
            style={{marginTop: '20px'}}
            onClick={event => { this.setState({ showCoreDevs: !this.state.showCoreDevs }) }}
          />
          <Button
            disabled={this.state.disableButtons}
            content='View Community Devs'
            primary={this.state.showCommDevs}
            secondary={!this.state.showCommDevs}
            style={{marginTop: '20px'}}
            onClick={event => { this.setState({ showCommDevs: !this.state.showCommDevs }) }}
          />
          </div>
          : null }


        {this.state.showCoreDevs ?
          <RenderDevelopers
            infoCommDevs={this.props.infoCommDevs}
            infoCoreDevs={this.props.infoCoreDevs}
            isCommDev={this.state.isCommDev}
            isCoreDev={this.state.isCoreDev}
            isManager={this.state.isManager}
            managerAddress={this.state.managerAddress}
            addressMetamask={this.state.addressMetamask}
            showCoreDevs={true}
            showTitle="Registered Core Developers"
          />
        : null }

        {this.state.showCommDevs ?
          <RenderDevelopers
            infoCommDevs={this.props.infoCommDevs}
            infoCoreDevs={this.props.infoCoreDevs}
            isCommDev={this.state.isCommDev}
            isCoreDev={this.state.isCoreDev}
            isManager={this.state.isManager}
            managerAddress={this.state.managerAddress}
            addressMetamask={this.state.addressMetamask}
            showCoreDevs={false}
            showTitle="Registered Community Developers"
          />
        : null }

      </Layout>
    );
  }
}

export default CampaignIndex;
