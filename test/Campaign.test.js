/******************************************************************************/
//
//  EGEM Crowdfunding - test/Campaign.test.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
const assert  = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());

const compiledFactory  = require('../Egem/build/CampaignFactory.json');
const compiledCampaign = require('../Egem/build/Campaign.json');

let accounts;
let factory;
let campaignAddress;
let campaign;
let managerAddress;
let managerNickname = 'Manager';
let minimumContribution = '100';
let lotteryPercentage = '10';

// Tests using ContractFactory account[0] is MANAGER
describe('Test CampaignFactory Contract -- Tests using Manager managerAddress', () => {
  beforeEach(async () => {
    accounts = await web3.eth.getAccounts();
    managerAddress = accounts[0];

    // managerAddress is the Factory owner
    factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
      .deploy({ data: compiledFactory.bytecode })
      .send({ from: managerAddress, gas: '6000000' });

    // managerAddress (Manager) creates a new campaign
    await factory.methods.createCampaign(minimumContribution,'Summary',managerNickname,'URL','10').send ({
      from: managerAddress,
      gas: '2000000'
    });

    [campaignAddress] = await factory.methods.getDeployedCampaigns().call();
    campaign = await new web3.eth.Contract(
      JSON.parse(compiledCampaign.interface),
      campaignAddress
    );
  });

  it('managerAddress deploys a factory and a campaign', async () => {
    assert.ok(factory.options.address);
    assert.ok(campaign.options.address);
    const manager = await factory.methods.manager().call();
    assert.equal(managerAddress, manager);
  });

  it('Manager nickname is flagged as used', async () => {
    const isUsed = await factory.methods.checkNickname(managerNickname).call();
    assert(isUsed);
  });

  it('contract address is stored in the CampaignFactory variable contractAddress', async () => {
    const contractAddress = await factory.methods.contractAddress().call();
    assert.equal(factory.options.address, contractAddress);
  });

  it('verify deployed campaigns', async () => {
    // There must be one campaign deployed
    var countCampaigns = await factory.methods.getCampaignsCount().call();
    assert.equal(countCampaigns, 1);
    // Check indexCampaign for first campaign is zero
    var index = await factory.methods.indexCampaign(campaignAddress).call();
    assert.equal(index,0);

    // Let number of campaigns be up to 5
    await factory.methods.setMaxCampaigns(5).send ({
      from: managerAddress,
      gas: '2000000'
    });

    // Lets now test adding 4 more compaigns and check
    for (var i=0; i<4; i++) {
      await factory.methods.createCampaign('100','Summary',managerNickname,'URL','10').send ({
        from: managerAddress,
        gas: '2000000'
      });
    }
    countCampaigns = await factory.methods.getCampaignsCount().call();
    assert.equal(countCampaigns, 5);
    var campaignAdd = await factory.methods.getDeployedCampaigns().call();
    var camp;
    for (var i=1; i<5; i++) {
      camp = await new web3.eth.Contract(
        JSON.parse(compiledCampaign.interface),
        campaignAdd[i]
      );
      assert.ok(camp.options.address);
      // Check indexCampaign for second campaign is one
      var index = await factory.methods.indexCampaign(campaignAdd[i]).call();
      assert.equal(index,i);
    }
  });

  it('verify infoCampaigns struct data is correct', async () => {
    const infoCampaigns = await factory.methods.infoCampaigns(0).call();
    assert.equal(infoCampaigns.campaignOwner,managerAddress);
    assert.equal(infoCampaigns.summaryInfo,'Summary');
    assert.equal(infoCampaigns.ownerNickname,managerNickname);
    assert.equal(infoCampaigns.urlProject,'URL');
    assert(!infoCampaigns.ownerFinish);
    assert(!infoCampaigns.campaignFinalized);
  });

  it('verify infoCoreDev struct data is correct', async () => {
    const infoCoreDevs = await factory.methods.infoCoreDevs(0).call();
    assert.equal(infoCoreDevs.nickName,managerNickname);
    assert.equal(infoCoreDevs.addressCoreDev,managerAddress);
    assert(infoCoreDevs.enableCoreDev);
  });

  it('verify infoCommDev struct data is correct', async () => {
    const infoCommDevs = await factory.methods.infoCommDevs(0).call();
    assert.equal(infoCommDevs.nickName,managerNickname);
    assert.equal(infoCommDevs.addressCommDev,managerAddress);
    assert(infoCommDevs.enableCommDev);
  });

  it('verify there is only 1 Core Developer = managerAddress', async () => {
    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(managerAddress).call();
    const countCoreDevs = contractData[1];
    const indexCoreDevs = contractData[3];
    const isCoreDev = devData[0];
    assert.equal(countCoreDevs, '1');
    assert.equal(indexCoreDevs, '1');
    assert(isCoreDev);
  });

  it('verify there is only 1 Community Developer = managerAddress', async () => {
    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(managerAddress).call();
    const countCommDevs = contractData[2];
    const indexCommDevs = contractData[4];
    const isCommDev = devData[1];
    assert.equal(countCommDevs, '1');
    assert.equal(indexCommDevs, '1');
    assert(isCommDev);
  });

  it('verify managerAddress cannot be registered again as Core developer', async () => {
    try {
      await factory.methods.registerDev('Manager2', true).send ({
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    const contractData = await factory.methods.getContractData().call();
    const countCoreDevs = contractData[1];
    const indexCoreDevs = contractData[3];
    assert.equal(countCoreDevs, '1');
    assert.equal(indexCoreDevs, '1');
  });


  it('verify managerAddress cannot be registered again as Comm developer', async () => {
    try {
      await factory.methods.registerDev('Manager2', false).send ({
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    const contractData = await factory.methods.getContractData().call();
    const countCommDevs = contractData[2];
    const indexCommDevs = contractData[4];
    assert.equal(countCommDevs, '1');
    assert.equal(indexCommDevs, '1');
  });

  it('verify accounts[1] can request register as new Core developer', async () => {
    await factory.methods.registerDev('CoreDev2', true).send ({
      from: accounts[1],
      gas: '2000000'
    });
    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(accounts[1]).call();
    const countCoreDevs = contractData[1];
    const indexCoreDevs = contractData[3];
    const isCoreDev = devData[0];
    assert.equal(countCoreDevs, '1'); // CoreDev account not yet approved, so just 1
    assert.equal(indexCoreDevs, '2'); // CoreDev index count is now 2
    assert(!isCoreDev);
    // Verify infoCoreDev data is correct
    const infoCoreDevs = await factory.methods.infoCoreDevs(1).call();
    assert.equal(infoCoreDevs.nickName,'CoreDev2');
    assert.equal(infoCoreDevs.addressCoreDev,accounts[1]);
    assert(!infoCoreDevs.enableCoreDev);
  });

  it('verify accounts[1] can request to register as new Comm developer', async () => {
    await factory.methods.registerDev('CommDev2', false).send ({
      from: accounts[1],
      gas: '2000000'
    });
    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(accounts[1]).call();
    const countCommDevs = contractData[2];
    const indexCommDevs = contractData[4];
    const isCommDev = devData[1];
    assert.equal(countCommDevs, '1'); // CommDev account not yet approved, so just 1
    assert.equal(indexCommDevs, '2'); // CommDev index count is now 2
    assert(!isCommDev);
    // Verify infoCommDev data is correct
    const infoCommDevs = await factory.methods.infoCommDevs(1).call();
    assert.equal(infoCommDevs.nickName,'CommDev2');
    assert.equal(infoCommDevs.addressCommDev,accounts[1]);
    assert(!infoCommDevs.enableCommDev);
  });

  //---------------------------
  it('verify registered accounts[1] cannot create multiple requests as new Core developer', async () => {
    for (var i=0; i<4; i++) {
      var nick="CoreDev" + i;
      await factory.methods.registerDev(nick, true).send ({
        from: accounts[1],
        gas: '2000000'
      });
    }
    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(accounts[1]).call();
    const countCoreDevs = contractData[1];
    const indexCoreDevs = contractData[3];
    const isCoreDev = devData[0];
    assert.equal(countCoreDevs, '1'); // CoreDev account not yet approved, so just 1
    assert.equal(indexCoreDevs, '2'); // CoreDev index count is now 2
    assert(!isCoreDev);
    // Verify infoCoreDev data is correct
    const infoCoreDevs = await factory.methods.infoCoreDevs(1).call();
    assert.equal(infoCoreDevs.nickName,'CoreDev0');
    assert.equal(infoCoreDevs.addressCoreDev,accounts[1]);
    assert(!infoCoreDevs.enableCoreDev);
  });

  it('verify registered accounts[1] cannot create multiple requests as new Comm developer', async () => {
    for (var i=0; i<4; i++) {
      var nick="CommDev" + i;
      await factory.methods.registerDev(nick, false).send ({
        from: accounts[1],
        gas: '2000000'
      });
    }
    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(accounts[1]).call();
    const countCommDevs = contractData[2];
    const indexCommDevs = contractData[4];
    const isCommDev = devData[1];
    assert.equal(countCommDevs, '1'); // CommDev account not yet approved, so just 1
    assert.equal(indexCommDevs, '2'); // CommDev index count is now 2
    assert(!isCommDev);
    // Verify infoCommDev data is correct
    const infoCommDevs = await factory.methods.infoCommDevs(1).call();
    assert.equal(infoCommDevs.nickName,'CommDev0');
    assert.equal(infoCommDevs.addressCommDev,accounts[1]);
    assert(!infoCommDevs.enableCommDev);
  });

  it('verify deleted accounts[1] cannot create multiple requests as new Core developer', async () => {
    // Lets register a new Core developer
    await factory.methods.registerDev('CoreDev2', true).send ({
      from: accounts[1],
      gas: '2000000'
    });
    await factory.methods.approveCoreDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    var contractData = await factory.methods.getContractData().call();
    var devData = await factory.methods.getDevData(accounts[1]).call();
    var countCoreDevs = contractData[1];
    var indexCoreDevs = contractData[3];
    var isCoreDev = devData[0];
    var isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '2');
    assert.equal(indexCoreDevs, '2');
    assert(isCoreDev);
    assert(!isCoreDevDeleted);
    await factory.methods.deleteCoreDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[1]).call();
    countCoreDevs = contractData[1];
    indexCoreDevs = contractData[3];
    isCoreDev = devData[0];
    isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '1');
    assert.equal(indexCoreDevs, '2');
    assert(!isCoreDev);
    assert(isCoreDevDeleted);
    // accounts[1] sends multiple new registration requests as Core developer
    for (var i=0; i<4; i++) {
      var nick = "CoreDev2" + i;
      await factory.methods.registerDev(nick, true).send ({
        from: accounts[1],
        gas: '2000000'
      });
    }
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[1]).call();
    countCoreDevs = contractData[1];
    indexCoreDevs = contractData[3];
    isCoreDev = devData[0];
    isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '1');
    assert.equal(indexCoreDevs, '2');
    assert(!isCoreDev);
    assert(!isCoreDevDeleted); // This flag is now false as account registered back again
    // Verify infoCoreDev data is correct
    const infoCoreDevs = await factory.methods.infoCoreDevs(1).call();
    assert.equal(infoCoreDevs.nickName,'CoreDev2');
    assert.equal(infoCoreDevs.addressCoreDev,accounts[1]);
    assert(!infoCoreDevs.enableCoreDev);
  });

  it('verify deleted accounts[1] cannot create multiple requests as new Comm developer', async () => {
    // Lets register a new Comm developer
    await factory.methods.registerDev('CommDev2', false).send ({
      from: accounts[1],
      gas: '2000000'
    });
    await factory.methods.approveCommDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    var contractData = await factory.methods.getContractData().call();
    var devData = await factory.methods.getDevData(accounts[1]).call();
    var countCommDevs = contractData[2];
    var indexCommDevs = contractData[4];
    var isCommDev = devData[1];
    var isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '2');
    assert.equal(indexCommDevs, '2');
    assert(isCommDev);
    assert(!isCommDevDeleted);
    await factory.methods.deleteCommDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[1]).call();
    countCommDevs = contractData[2];
    indexCommDevs = contractData[4];
    isCommDev = devData[1];
    isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '1');
    assert.equal(indexCommDevs, '2');
    assert(!isCommDev);
    assert(isCommDevDeleted);
    // accounts[1] sends multiple registration requests as Comm developer
    for (var i=0; i<4; i++) {
      nick = "CommDev2" + i;
      await factory.methods.registerDev(nick, false).send ({
        from: accounts[1],
        gas: '2000000'
      });
    }
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[1]).call();
    countCommDevs = contractData[2];
    indexCommDevs = contractData[4];
    isCommDev = devData[1];
    isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '1');
    assert.equal(indexCommDevs, '2');
    assert(!isCommDev);
    assert(!isCommDevDeleted); // This flag is now false as account registered back again
    // Verify infoCommDev data is correct
    const infoCommDevs = await factory.methods.infoCommDevs(1).call();
    assert.equal(infoCommDevs.nickName,'CommDev2');
    assert.equal(infoCommDevs.addressCommDev,accounts[1]);
    assert(!infoCommDevs.enableCommDev);
  });
  //---------------------------

  it('verify managerAddress can approve accounts[1] as new Core developer', async () => {
    // accounts[1] send a request to register as a CoreDev.
    await factory.methods.registerDev('CoreDev2', true).send ({
      from: accounts[1],
      gas: '2000000'
    });
    // Manager approves the new CoreDev registration request
    await factory.methods.approveCoreDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(accounts[1]).call();
    const countCoreDevs = contractData[1];
    const indexCoreDevs = contractData[3];
    const isCoreDev = devData[0];
    assert.equal(countCoreDevs, '2');
    assert.equal(indexCoreDevs, '2');
    assert(isCoreDev);
    // Verify infoCoreDev data is correct
    const infoCoreDevs = await factory.methods.infoCoreDevs(1).call();
    assert.equal(infoCoreDevs.nickName,'CoreDev2');
    assert.equal(infoCoreDevs.addressCoreDev,accounts[1]);
    assert(infoCoreDevs.enableCoreDev);
  });

  it('verify managerAddress can approve accounts[1] as new Comm developer', async () => {
    // accounts[1] send a request to register as a CommDev.
    await factory.methods.registerDev('CommDev2', false).send ({
      from: accounts[1],
      gas: '2000000'
    });
    // Manager approves the new CommDev registration request
    await factory.methods.approveCommDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(accounts[1]).call();
    const countCommDevs = contractData[2];
    const indexCommDevs = contractData[4];
    const isCommDev = devData[1];
    assert.equal(countCommDevs, '2');
    assert.equal(indexCommDevs, '2');
    assert(isCommDev);
    // Verify infoCommDev data is correct
    const infoCommDevs = await factory.methods.infoCommDevs(1).call();
    assert.equal(infoCommDevs.nickName,'CommDev2');
    assert.equal(infoCommDevs.addressCommDev,accounts[1]);
    assert(infoCommDevs.enableCommDev);
  });

  it('verify managerAddress cannot delete itself from Core developer', async () => {
    try {
      await factory.methods.deleteCoreDev(managerAddress).send ({
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      // Execution of deleteCoreDev() should fail. This is the expected result
      assert(err);
    }

    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(managerAddress).call();
    const countCoreDevs = contractData[1];
    const indexCoreDevs = contractData[3];
    const isCoreDev = devData[0];
    const isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '1');
    assert.equal(indexCoreDevs, '1');
    assert(isCoreDev);
    assert(!isCoreDevDeleted);
    // Verify infoCoreDev data is still correct
    const infoCoreDevs = await factory.methods.infoCoreDevs(0).call();
    assert.equal(infoCoreDevs.nickName,managerNickname);
    assert.equal(infoCoreDevs.addressCoreDev,accounts[0]);
    assert(infoCoreDevs.enableCoreDev);
  });

  it('verify managerAddress cannot delete itself from Community developer', async () => {
    try {
      await factory.methods.deleteCommDev(managerAddress).send ({
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      // Execution of deleteCommDev() should fail. This is the expected result
      assert(err);
    }

    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(managerAddress).call();
    const countCommDevs = contractData[2];
    const indexCommDevs = contractData[4];
    const isCommDev = devData[1];
    const isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '1');
    assert.equal(indexCommDevs, '1');
    assert(isCommDev);
    assert(!isCommDevDeleted);
    // Verify infoCommDev data is correct
    const infoCommDevs = await factory.methods.infoCommDevs(0).call();
    assert.equal(infoCommDevs.nickName,managerNickname);
    assert.equal(infoCommDevs.addressCommDev,accounts[0]);
    assert(infoCommDevs.enableCommDev);
  });

  it('verify managerAddress can delete another Core developer', async () => {
    // Lets register a new Core developer
    await factory.methods.registerDev('CoreDev2', true).send ({
      from: accounts[1],
      gas: '2000000'
    });
    await factory.methods.approveCoreDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    var contractData = await factory.methods.getContractData().call();
    var devData = await factory.methods.getDevData(accounts[1]).call();
    var countCoreDevs = contractData[1];
    var indexCoreDevs = contractData[3];
    var isCoreDev = devData[0];
    var isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '2');
    assert.equal(indexCoreDevs, '2');
    assert(isCoreDev);
    assert(!isCoreDevDeleted);
    await factory.methods.deleteCoreDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[1]).call();
    countCoreDevs = contractData[1];
    indexCoreDevs = contractData[3];
    isCoreDev = devData[0];
    isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '1');
    assert.equal(indexCoreDevs, '2');
    assert(!isCoreDev);
    assert(isCoreDevDeleted);
    // Verify infoCoreDev data is still correct and updated
    const infoCoreDevs = await factory.methods.infoCoreDevs(1).call();
    assert.equal(infoCoreDevs.nickName,'CoreDev2');
    assert.equal(infoCoreDevs.addressCoreDev,accounts[1]);
    assert(!infoCoreDevs.enableCoreDev); // Not enabled anymore
  });

  it('verify managerAddress can approve back a deleted Core developer', async () => {
    // Lets register a new Core developer
    await factory.methods.registerDev('CoreDev2', true).send ({
      from: accounts[1],
      gas: '2000000'
    });
    await factory.methods.approveCoreDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    var contractData = await factory.methods.getContractData().call();
    var devData = await factory.methods.getDevData(accounts[1]).call();
    var countCoreDevs = contractData[1];
    var indexCoreDevs = contractData[3];
    var isCoreDev = devData[0];
    var isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '2');
    assert.equal(indexCoreDevs, '2');
    assert(isCoreDev);
    assert(!isCoreDevDeleted);
    await factory.methods.deleteCoreDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[1]).call();
    countCoreDevs = contractData[1];
    indexCoreDevs = contractData[3];
    isCoreDev = devData[0];
    isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '1');
    assert.equal(indexCoreDevs, '2');
    assert(!isCoreDev);
    assert(isCoreDevDeleted);
    // accounts[1] sends a new registration request as Core developer
    await factory.methods.registerDev('CoreDev2', true).send ({
      from: accounts[1],
      gas: '2000000'
    });
    await factory.methods.approveCoreDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[1]).call();
    countCoreDevs = contractData[1];
    indexCoreDevs = contractData[3];
    isCoreDev = devData[0];
    isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '2');
    assert.equal(indexCoreDevs, '2');
    assert(isCoreDev);
    assert(!isCoreDevDeleted);
    // Verify infoCoreDev data is correct
    const infoCoreDevs = await factory.methods.infoCoreDevs(1).call();
    assert.equal(infoCoreDevs.nickName,'CoreDev2');
    assert.equal(infoCoreDevs.addressCoreDev,accounts[1]);
    assert(infoCoreDevs.enableCoreDev);
  });

  it('verify managerAddress can delete another Comm developer', async () => {
    // Lets register a new Core developer
    await factory.methods.registerDev('CommDev2', false).send ({
      from: accounts[1],
      gas: '2000000'
    });
    await factory.methods.approveCommDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    var contractData = await factory.methods.getContractData().call();
    var devData = await factory.methods.getDevData(accounts[1]).call();
    var countCommDevs = contractData[2];
    var indexCommDevs = contractData[4];
    var isCommDev = devData[1];
    var isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '2');
    assert.equal(indexCommDevs, '2');
    assert(isCommDev);
    assert(!isCommDevDeleted);
    await factory.methods.deleteCommDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[1]).call();
    countCommDevs = contractData[2];
    indexCommDevs = contractData[4];
    isCommDev = devData[1];
    isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '1');
    assert.equal(indexCommDevs, '2');
    assert(!isCommDev);
    assert(isCommDevDeleted);
    // Verify infoCommDev data is correct
    const infoCommDevs = await factory.methods.infoCommDevs(1).call();
    assert.equal(infoCommDevs.nickName,'CommDev2');
    assert.equal(infoCommDevs.addressCommDev,accounts[1]);
    assert(!infoCommDevs.enableCommDev);
  });

  it('verify managerAddress can approve back a deleted Comm developer', async () => {
    // Lets register a new Comm developer
    await factory.methods.registerDev('CommDev2', false).send ({
      from: accounts[1],
      gas: '2000000'
    });
    await factory.methods.approveCommDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    var contractData = await factory.methods.getContractData().call();
    var devData = await factory.methods.getDevData(accounts[1]).call();
    var countCommDevs = contractData[2];
    var indexCommDevs = contractData[4];
    var isCommDev = devData[1];
    var isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '2');
    assert.equal(indexCommDevs, '2');
    assert(isCommDev);
    assert(!isCommDevDeleted);
    await factory.methods.deleteCommDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[1]).call();
    countCommDevs = contractData[2];
    indexCommDevs = contractData[4];
    isCommDev = devData[1];
    isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '1');
    assert.equal(indexCommDevs, '2');
    assert(!isCommDev);
    assert(isCommDevDeleted);
    // accounts[1] sends a nre registration request as Comm developer
    await factory.methods.registerDev('CommDev2', false).send ({
      from: accounts[1],
      gas: '2000000'
    });
    await factory.methods.approveCommDev(accounts[1]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[1]).call();
    countCommDevs = contractData[2];
    indexCommDevs = contractData[4];
    isCommDev = devData[1];
    isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '2');
    assert.equal(indexCommDevs, '2');
    assert(isCommDev);
    assert(!isCommDevDeleted);
    // Verify infoCommDev data is correct
    const infoCommDevs = await factory.methods.infoCommDevs(1).call();
    assert.equal(infoCommDevs.nickName,'CommDev2');
    assert.equal(infoCommDevs.addressCommDev,accounts[1]);
    assert(infoCommDevs.enableCommDev);
  });

  // NEW STUFF

  it('verify managerAddress can finalize a campaign', async () => {
    try {
      await factory.methods.finalizeCampaign(campaignAddress).send ({
        from: managerAddress,
        gas: '2000000'
      });
    } catch (err) {
      assert(false);
    }

    const isFinalized = await factory.methods.campaignFinalized(campaignAddress).call();
    assert(isFinalized);
    const infoCampaigns = await factory.methods.infoCampaigns(0).call();
    assert(infoCampaigns.isFinalized);
  });

  it('verify Campaign owner can finalize a campaign', async () => {
    // Campaign owner finalizes the Campaign
    var infoCampaigns = await factory.methods.infoCampaigns(0).call();
    const campaignOwner = infoCampaigns.campaignOwner;
    try {
      await factory.methods.finalizeCampaign(campaignAddress).send ({
        from: campaignOwner,
        gas: '2000000'
      });
    } catch (err) {
      assert(false);
    }

    const isFinalized = await factory.methods.campaignFinalized(campaignAddress).call();
    assert(isFinalized);
    infoCampaigns = await factory.methods.infoCampaigns(0).call();
    assert(infoCampaigns.isFinalized);
  });

  it('verify other Address cannot finalize a campaign', async () => {
    // Campaign owner finalizes the Campaign
    const otherAddress = accounts[5];
    try {
      await factory.methods.finalizeCampaign(campaignAddress).send ({
        from: otherAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }

    const isFinalized = await factory.methods.campaignFinalized(campaignAddress).call();
    assert(!isFinalized);
    infoCampaigns = await factory.methods.infoCampaigns(0).call();
    assert(!infoCampaigns.isFinalized);
  });

  it('verify managerAddress can make active again an already finalized campaign', async () => {
    // Campaign owner finalizes the Campaign
    var infoCampaigns = await factory.methods.infoCampaigns(0).call();
    const campaignOwner = infoCampaigns.campaignOwner;
    try {
      await factory.methods.finalizeCampaign(campaignAddress).send ({
        from: campaignOwner,
        gas: '2000000'
      });
    } catch (err) {
      assert(false);
    }
    // Make sure it is finalized
    var isFinalized = await factory.methods.campaignFinalized(campaignAddress).call();
    assert(isFinalized);
    infoCampaigns = await factory.methods.infoCampaigns(0).call();
    assert(infoCampaigns.isFinalized);
    // Lets make it back active
    try {
      await factory.methods.enableCampaign(campaignAddress).send ({
        from: managerAddress,
        gas: '2000000'
      });
    } catch (err) {
      assert(false);
    }
    isFinalized = await factory.methods.campaignFinalized(campaignAddress).call();
    assert(!isFinalized);
    infoCampaigns = await factory.methods.infoCampaigns(0).call();
    assert(!infoCampaigns.isFinalized);
  });

  it('verify it is not possible to finalize a Campaign with remaining funds', async () => {
    // Campaign will have 500 Wei
    const contribute = '500';
    const anyAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: anyAddress,
      gas: '2000000'
    });
    // Verify Campaing has funds.
    const summary = await campaign.methods.getSummary().call();
    assert(summary[1].valueOf() != 0);
    try {
      await factory.methods.finalizeCampaign(campaignAddress).send ({
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    const isFinalized = await factory.methods.campaignFinalized(campaignAddress).call();
    assert(!isFinalized);
    const infoCampaigns = await factory.methods.infoCampaigns(0).call();
    assert(!infoCampaigns.isFinalized);
  });

  it('verify managerAddress cannot request to become approver of a finalized campaign', async () => {
    try {
      await factory.methods.finalizeCampaign(campaignAddress).send ({
        from: managerAddress,
        gas: '2000000'
      });
    } catch (err) {
      assert(false);
    }

    try {
      await campaign.methods.campaignApprover().send({
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('verify managerAddress cannot contribute funds to a finalized campaign', async () => {
    try {
      await factory.methods.finalizeCampaign(campaignAddress).send ({
        from: managerAddress,
        gas: '2000000'
      });
    } catch (err) {
      assert(false);
    }

    try {
      const contribution = '200';
      await campaign.methods.contribute().send({
        value: contribution,
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('verify managerAddress can set Campaign to Refund state', async () => {
    try {
      await campaign.methods.refundCampaign().send({
        from: managerAddress,
        gas: '2000000'
      });
    } catch (err) {
      assert(false);
    }
    const isRefundState = await campaign.methods.refundedCampaign().call();
    assert(isRefundState);
  });

  it('verify other Address cannot set Campaign to Refund state', async () => {
    const otherAddress = accounts[5];
    try {
      await campaign.methods.refundCampaign().send({
        from: otherAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    const isRefundState = await campaign.methods.refundedCampaign().call();
    assert(!isRefundState);
  });

  it('verify accounts get funds back from Campaign in Refund state', async () => {
    // Lets begin by making 9 contributions to the Campaign
    const contribute = web3.utils.toWei('1.0', 'ether');
    var prevBalance = [];
    var newBalance = [];
    for (var i = 1; i < 10; i++) {
      var balance = await web3.eth.getBalance(accounts[i]);
      var balanceEth = web3.utils.fromWei(balance, 'ether');
      prevBalance[i] = balanceEth;
      await campaign.methods.contribute().send({
        value: contribute,
        from: accounts[i],
        gas: '2000000'
      });
    }
    try {
      await campaign.methods.refundCampaign().send({
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    const isRefundState = await campaign.methods.refundedCampaign().call();
    assert(isRefundState);
    for (var i = 1; i < 10; i++) {
      await campaign.methods.getRefund().send({
        from: accounts[i],
        gas: '2000000'
      });
      var balance = await web3.eth.getBalance(accounts[i]);
      var balanceEth = web3.utils.fromWei(balance, 'ether');
      newBalance[i] = balanceEth;
      var diffBalance = prevBalance[i] - newBalance[i];
      assert(parseFloat(diffBalance) < 0.1);
    }
  });
});

describe('##### Test Campaign Contract ##### - Tests using Manager managerAddress', () => {
  beforeEach(async () => {
    accounts = await web3.eth.getAccounts();
    managerAddress = accounts[0];

    // managerAddress is the Factory owner
    factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
      .deploy({ data: compiledFactory.bytecode })
      .send({ from: managerAddress, gas: '6000000' });

    // managerAddress (Manager) creates a new campaign
    await factory.methods.createCampaign(minimumContribution,'Summary',managerNickname,'URL','10').send ({
      from: managerAddress,
      gas: '2000000'
    });

    [campaignAddress] = await factory.methods.getDeployedCampaigns().call();
    campaign = await new web3.eth.Contract(
      JSON.parse(compiledCampaign.interface),
      campaignAddress
    );
  });

  it('New Campaign instance, in this test the campaign manager is managerAddress', async () => {
    const manager = await campaign.methods.manager().call();
    assert.equal(managerAddress, manager);
  });

  it('New Campaign instance minimumContribution is correct', async () => {
    const minimum = await campaign.methods.minimumContribution().call();
    assert.equal(minimum, minimumContribution);
  });

  it('New Campaign instance managerAddress is in the list of approvers', async () => {
    const isApprover = await campaign.methods.approvers(managerAddress).call();
    assert(isApprover);
  });

  it('New Campaign instance approversCount is initially 1', async () => {
    const approversCount = await campaign.methods.approversCount().call();
    assert.equal(approversCount,1);
  });

  it('New Campaign instance supportersCount is initially 0', async () => {
    const supportersCount = await campaign.methods.supportersCount().call();
    assert.equal(supportersCount,0);
  });

  it('New Campaign instance does not have any withdrawals requests', async () => {
    const requestsCount = await campaign.methods.getRequestsCount().call();
    assert.equal(requestsCount,0);
  });

  it('Manager can contribute funds to Campaign - related Campaign state parameters are updated', async () => {
    // Contribute 200 Wei
    const contribution = '200';
    await campaign.methods.contribute().send({
      value: contribution,
      from: managerAddress,
      gas: '2000000'
    });
    // Supporters count is now incremented to 1
    const supportersCount = await campaign.methods.supportersCount().call();
    assert.equal(supportersCount,1);
    // We keep track of funds provided by managerAddress
    const amount = await campaign.methods.fundsRcvd(managerAddress).call();
    assert.equal(amount, contribution);
    // Campaign balance equal to contribution
    const summary = await campaign.methods.getSummary().call();
    assert.equal(summary[1], contribution);
  });

  it('Other address can contribute funds to Campaign - related Campaign state parameters are updated', async () => {
    const otherAddress = accounts[5];
    // Contribute 200 Wei
    const contribution = '200';
    await campaign.methods.contribute().send({
      value: contribution,
      from: otherAddress,
      gas: '2000000'
    });
    // Supporters count is now incremented to 1
    const supportersCount = await campaign.methods.supportersCount().call();
    assert.equal(supportersCount,1);
    // We keep track of funds provided by managerAddress
    const amount = await campaign.methods.fundsRcvd(otherAddress).call();
    assert.equal(amount, contribution);
    // Campaign balance equal to contribution
    const summary = await campaign.methods.getSummary().call();
    assert.equal(summary[1], contribution);
  });

  it('Manager cannot contribut less than minimum contribution', async () => {
    const lessContrib = minimumContribution.valueOf() - 1;
    try {
      await campaign.methods.contribute().send({
        value: lessContrib,
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch(err) {
      // function call must fail, this is the expected behavior.
      assert(err);
    }
  });

  it('Other address cannot contribut less than minimum contribution', async () => {
    const otherAddress = accounts[5];
    const lessContrib = minimumContribution.valueOf() - 1;
    try {
      await campaign.methods.contribute().send({
        value: lessContrib,
        from: otherAddress,
        gas: '2000000'
      });
      assert(false);
    } catch(err) {
      // function call must fail, this is the expected behavior.
      assert(err);
    }
  });

  it('Manager cannot create a withdrawal request if Campaign hasn\'t enough funds', async () => {
    // Campaign will have 500 Wei
    const contribute = '500';
    const anyAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: anyAddress,
      gas: '2000000'
    });
    // Manager creates a withdrawal request gratter than current Campaign balance
    const amount = contribute.valueOf() + 1;
    try {
      await campaign.methods
        .createRequest('Buy batteries', amount, managerAddress)
        .send({
          from: managerAddress,
          gas: '2000000'
        });
      assert(false);
    } catch (err) {
      assert(err);
    }
    // Number of withdrawal request must be 0
    const requestsCount = await campaign.methods.getRequestsCount().call();
    assert.equal(requestsCount,0);
  });

  it('Manager can create a withdrawal request if Campaign has enough funds', async () => {
    // Campaign will have 500 Wei
    const contribute = '500';
    const anyAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: anyAddress,
      gas: '2000000'
    });
    // Manager creates a withdrawal request
    const amount = contribute.valueOf();
    await campaign.methods
      .createRequest('Buy batteries', amount, managerAddress)
      .send({
        from: managerAddress,
        gas: '2000000'
      });
    // Number or withdrawal requests must be 1
    const requestsCount = await campaign.methods.getRequestsCount().call();
    assert.equal(requestsCount, 1);
    // Lets check request data
    const request = await campaign.methods.requests(0).call();
    assert.equal('Buy batteries', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(managerAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '0');
  });

  it('other Address cannot create a withdrawal request', async () => {
    // Campaign will have 500 Wei
    const contribute = '500';
    const otherAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: otherAddress,
      gas: '2000000'
    });
    try {
      // otherAddress tries to create a withdrawal request
      const amount = contribute.valueOf();
      await campaign.methods
        .createRequest('Buy batteries', amount, otherAddress)
        .send({
          from: otherAddress,
          gas: '2000000'
        });
      assert(false);
    } catch (err) {
      assert(err);
    }
    // Number of withdrawal request must be 0
    const requestsCount = await campaign.methods.getRequestsCount().call();
    assert.equal(requestsCount,0);
  });

  it('Manager approves a withdrawal request', async () => {
    // Campaign will have 10 Ether
    const contribute = web3.utils.toWei('10', 'ether');
    const otherAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: otherAddress,
      gas: '2000000'
    });

    let prevBalance = await web3.eth.getBalance(managerAddress);
    balance = web3.utils.fromWei(prevBalance, 'ether');
    prevBalance = parseFloat(balance);

    // Campaign owner creates a withdrawal request
    const amount = web3.utils.toWei('5', 'ether');
    await campaign.methods
      .createRequest('A', amount, managerAddress)
      .send({
        from: managerAddress,
        gas: '2000000'
      });
    // Lets check request data
    var request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(managerAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '0');

    // A campaign approver aproves the withdrawal request
    await campaign.methods.approveRequest(0).send({
      from: managerAddress,
      gas: '2000000'
    });
    // Lets check request data
    request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(managerAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '1');

    // The request owner finilizes the withdrawal request.
    await campaign.methods.finalizeRequest(0).send({
      from: managerAddress,
      gas: '2000000'
    });
    // Lets check request data
    request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(managerAddress, request.recipient);
    assert(request.complete);
    assert.equal(request.approvalCount, '1');

    let newBalance = await web3.eth.getBalance(managerAddress);
    balance = web3.utils.fromWei(newBalance, 'ether');
    newBalance = parseFloat(balance);
    assert(newBalance > prevBalance);
  });

  it('Approver will fail to approve a second time a withdrawal request.', async () => {
    // Campaign will have 10 Ether
    const contribute = web3.utils.toWei('10', 'ether');
    const otherAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: otherAddress,
      gas: '2000000'
    });

    // Campaign owner creates a withdrawal request
    const amount = web3.utils.toWei('5', 'ether')
    await campaign.methods
      .createRequest('A', amount, managerAddress)
      .send({
        from: managerAddress,
        gas: '2000000'
      });
    // Lets check request data
    var request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(managerAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '0');

    // A campaign approver aproves the withdrawal request the first time
    try {
      await campaign.methods.approveRequest(0).send({
        from: managerAddress,
        gas: '2000000'
      });
    } catch (err) {
      assert(false);
    }
    // Lets check request data
    request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(managerAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '1');

    // The same approver fails to approve the withdrawal request the second time
    try {
      await campaign.methods.approveRequest(0).send({
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('other Address cannot approve a withdrawal request', async () => {
    // Campaign will have 10 Ether
    const contribute = web3.utils.toWei('10', 'ether');
    const otherAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: otherAddress,
      gas: '2000000'
    });

    // Campaign owner creates a withdrawal request
    const amount = web3.utils.toWei('5', 'ether');
    await campaign.methods
      .createRequest('A', amount, managerAddress)
      .send({
        from: managerAddress,
        gas: '2000000'
      });
    // Lets check request data
    var request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(managerAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '0');

    // Other address tries to aprove the withdrawal request
    try {
      await campaign.methods.approveRequest(0).send({
        from: otherAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      // It should fail, so test can pass
      assert(err);
    }
    // Lets check request data
    request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(managerAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '0');
  });

  it('otherAddress fails to finalize a withdrawal request', async () => {
    // Campaign will have 10 Ether
    const contribute = web3.utils.toWei('10', 'ether');
    const otherAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: otherAddress,
      gas: '2000000'
    });

    // Campaign owner creates a withdrawal request
    const amount = web3.utils.toWei('5', 'ether');
    await campaign.methods
      .createRequest('A', amount, managerAddress)
      .send({
        from: managerAddress,
        gas: '2000000'
      });
    // Lets check request data
    var request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(managerAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '0');

    // A campaign approver aproves the withdrawal request
    await campaign.methods.approveRequest(0).send({
      from: managerAddress,
      gas: '2000000'
    });
    // Lets check request data
    request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(managerAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '1');

    try {
      await campaign.methods.finalizeRequest(0).send({
        from: otherAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });
});
