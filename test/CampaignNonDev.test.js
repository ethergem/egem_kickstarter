/******************************************************************************/
//
//  EGEM Crowdfunding - test/CampaignNonDev.test.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
const assert  = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());

const compiledFactory  = require('../Egem/build/CampaignFactory.json');
const compiledCampaign = require('../Egem/build/Campaign.json');

let accounts;
let factory;
let campaignAddress;
let campaign;
let managerAddress;
let coredevAddress;
let anyAddress;
let managerNickname="Manager";
let coreDevNickname="CoreDev";
let anyNickname="User";
let minimumContribution = '100';

// Tests using ContractFactory account[0] is MANAGER
describe('Test CampaignFactory Contract -- Tests using Non-Core/Comm dev account', () => {
  beforeEach(async () => {
    accounts = await web3.eth.getAccounts();
    managerAddress = accounts[0];
    coredevAddress = accounts[1];
    anyAddress = accounts[3];

    // managerAddress is the Factory owner
    factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
      .deploy({ data: compiledFactory.bytecode })
      .send({ from: managerAddress, gas: '6000000' });

    // accounts[1] send a request to register as a CoreDev.
    await factory.methods.registerDev(coreDevNickname, true).send ({
      from: coredevAddress,
      gas: '2000000'
    });
    // Manager approves the new CoreDev registration request
    await factory.methods.approveCoreDev(coredevAddress).send ({
      from: managerAddress,
      gas: '2000000'
    });

    // accounts[1] (CoreDev) creates a new campaign
    await factory.methods.createCampaign(minimumContribution,'Summary',coreDevNickname,'URL','10').send ({
      from: coredevAddress,
      gas: '2000000'
    });

    [campaignAddress] = await factory.methods.getDeployedCampaigns().call();
    campaign = await new web3.eth.Contract(
      JSON.parse(compiledCampaign.interface),
      campaignAddress
    );
  });

  it('Non-Core/Comm cannot deployed a campaign', async () => {
    try {
      // anyAddress tries to creates a new campaign
      await factory.methods.createCampaign('100','Summary',anyNickname,'URL','10').send ({
        from: anyAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    var countCampaigns = await factory.methods.getCampaignsCount().call();
    assert.equal(countCampaigns, 1);
  });

  it('verify anyAddress cannot approve accounts[2] as new Core developer', async () => {
    // accounts[2] send a request to register as a CoreDev.
    await factory.methods.registerDev('CoreDev2', true).send ({
      from: accounts[2],
      gas: '2000000'
    });
    try {
      // anyAddress tries to approve the new CoreDev registration request
      await factory.methods.approveCoreDev(accounts[2]).send ({
        from: anyAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('verify anyAddress cannot approve accounts[2] as new Comm developer', async () => {
    // accounts[2] send a request to register as a CommDev.
    await factory.methods.registerDev('CommDev2', false).send ({
      from: accounts[2],
      gas: '2000000'
    });
    try {
      // Manager approves the new CommDev registration request
      await factory.methods.approveCommDev(accounts[2]).send ({
        from: anyAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('verify anyAddress cannot delete another Core developer', async () => {
    // Lets register a new Core developer
    await factory.methods.registerDev('CoreDev2', true).send ({
      from: accounts[2],
      gas: '2000000'
    });
    await factory.methods.approveCoreDev(accounts[2]).send ({
      from: coredevAddress,
      gas: '2000000'
    });
    var contractData = await factory.methods.getContractData().call();
    var devData = await factory.methods.getDevData(accounts[2]).call();
    var countCoreDevs = contractData[1];
    var indexCoreDevs = contractData[3];
    var isCoreDev = devData[0];
    var isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '3');
    assert.equal(indexCoreDevs, '3');
    assert(isCoreDev);
    assert(!isCoreDevDeleted);
    try {
      await factory.methods.deleteCoreDev(accounts[2]).send ({
        from: anyAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[2]).call();
    countCoreDevs = contractData[1];
    indexCoreDevs = contractData[3];
    isCoreDev = devData[0];
    isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '3');
    assert.equal(indexCoreDevs, '3');
    assert(isCoreDev);
    assert(!isCoreDevDeleted);
    // Verify infoCoreDev data is still correct and updated
    const infoCoreDevs = await factory.methods.infoCoreDevs(2).call();
    assert.equal(infoCoreDevs.nickName,'CoreDev2');
    assert.equal(infoCoreDevs.addressCoreDev,accounts[2]);
    assert(infoCoreDevs.enableCoreDev);
  });

  it('verify anyAddress cannot delete another Comm developer', async () => {
    // Lets register a new Comm developer
    await factory.methods.registerDev('CommDev2', false).send ({
      from: accounts[2],
      gas: '2000000'
    });
    await factory.methods.approveCommDev(accounts[2]).send ({
      from: coredevAddress,
      gas: '2000000'
    });
    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(accounts[2]).call();
    const countCommDevs = contractData[2];
    const indexCommDevs = contractData[4];
    const isCommDev = devData[1];
    const isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '2'); // Only 2 ass accounts[1] has not been authorized
    assert.equal(indexCommDevs, '2');
    assert(isCommDev);
    assert(!isCommDevDeleted);
    try {
      await factory.methods.deleteCommDev(accounts[2]).send ({
        from: anyAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });
});
