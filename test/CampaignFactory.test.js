/******************************************************************************/
//
//  EGEM Crowdfunding - test/CampaignFactory.test.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
const assert  = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());

const compiledFactory  = require('../Egem/build/CampaignFactory.json');
const compiledCampaign = require('../Egem/build/Campaign.json');

let accounts;
let factory;
let campaignAddress;
let campaign;
let managerAddress;
let managerNickname="Manager";

// Tests using ContractFactory account[0] is MANAGER
describe('Test CampaignFactory Contract -- verify contractEnable use', () => {
  beforeEach(async () => {
    accounts = await web3.eth.getAccounts();
    managerAddress = accounts[0];

    // managerAddress is the Factory owner
    factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
      .deploy({ data: compiledFactory.bytecode })
      .send({ from: managerAddress, gas: '6000000' });

    // managerAddress (Manager) creates a new campaign
    await factory.methods.createCampaign('100','Summary','Nickname','URL','10').send ({
      from: managerAddress,
      gas: '2000000'
    });

    [campaignAddress] = await factory.methods.getDeployedCampaigns().call();
    campaign = await new web3.eth.Contract(
      JSON.parse(compiledCampaign.interface),
      campaignAddress
    );
  });

  it('contract address is stored in the CampaignFactory variable contractAddress', async () => {
    const contractAddress = await factory.methods.contractAddress().call();
    assert.equal(factory.options.address, contractAddress);
  });

  it('CampaignFactory contractEnabled is true when campaignFactory is deployed', async () => {
    const contractData = await factory.methods.getContractData().call();
    const contractEnable = contractData[0];
    assert.equal(contractEnable, true);
  });

  it('CampaignFactory contractEnabled is false when trigger', async () => {
    var contractData = await factory.methods.getContractData().call();
    const contractEnable = contractData[0];
    assert.equal(contractEnable, true);
    await factory.methods.setContractStatus(false).send ({
      from: managerAddress,
      gas: '3000000'
    });
    try {
      contractData = await factory.methods.getContractData().call();
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('contractEnabled is false no function can be executed', async () => {
    var contractData = await factory.methods.getContractData().call();
    const contractEnable = contractData[0];
    assert.equal(contractEnable, true);
    await factory.methods.setContractStatus(false).send ({
      from: managerAddress,
      gas: '3000000'
    });
    try {
      contractData = await factory.methods.getContractData().call();
      assert(false);
    } catch (err) {
      assert(err);
    }
    try {
      await factory.methods.createCampaign('100','Summary','Nickname','URL','10').send ({
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    try {
      var campaignAdd = await factory.methods.getDeployedCampaigns().call();
      assert(false);
    } catch (err) {
      assert(err);
    }
    try {
      var countCampaigns = await factory.methods.getCampaignsCount().call();
      assert(false);
    } catch (err) {
      assert(err);
    }
    try {
      await factory.methods.registerDev('CoreDev2', true).send ({
        from: accounts[1],
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    try {
      await factory.methods.deleteCoreDev('0xA263360F32e2B1116E16E755050a5de4C41B39ed').send ({
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    try {
      await factory.methods.deleteCommDev('0xA263360F32e2B1116E16E755050a5de4C41B39ed').send ({
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    try {
      await factory.methods.approveCoreDev(accounts[0]).send ({
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    try {
      await factory.methods.approveCommDev(accounts[0]).send ({
        from: managerAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    try {
      const isUsed = await factory.methods.checkNickname(managerNickname).call();
      assert(false);
    } catch (err) {
      assert(err);
    }
    try {
      // FINALLY set contractEnable back to true
      await factory.methods.setContractStatus(true).send ({
        from: managerAddress,
        gas: '2000000'
      });
      const contractData = await factory.methods.getContractData().call();
      const contractEnable = contractData[0];
      assert.equal(contractEnable, true);
    } catch (err) {
      assert(false);
    }
  });
});
