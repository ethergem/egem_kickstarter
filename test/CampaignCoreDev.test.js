/******************************************************************************/
//
//  EGEM Crowdfunding - test/CampaignCoreDev.test.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
const assert  = require('assert');
const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());

const compiledFactory  = require('../Egem/build/CampaignFactory.json');
const compiledCampaign = require('../Egem/build/Campaign.json');

let accounts;
let factory;
let campaignAddress;
let campaign;
let managerAddress;
let coredevAddress;
let managerNickname="Manager";
let coreDevNickname="CoreDev";
let minimumContribution = '100';
let lotteryPercentage = '10';

// Tests using ContractFactory account[0] is MANAGER
describe('Test CampaignFactory Contract -- Tests using CoreDev accounts[1]', () => {
  beforeEach(async () => {
    accounts = await web3.eth.getAccounts();
    managerAddress = accounts[0];
    coredevAddress = accounts[1];

    // managerAddress is the Factory owner
    factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
      .deploy({ data: compiledFactory.bytecode })
      .send({ from: managerAddress, gas: '6000000' });

    // accounts[1] send a request to register as a CoreDev.
    await factory.methods.registerDev(coreDevNickname, true).send ({
      from: coredevAddress,
      gas: '2000000'
    });
    // Manager approves the new CoreDev registration request
    await factory.methods.approveCoreDev(coredevAddress).send ({
      from: managerAddress,
      gas: '2000000'
    });

    // accounts[1] (CoreDev) creates a new campaign
    await factory.methods.createCampaign(minimumContribution,'Summary',coreDevNickname,'URL','10').send ({
      from: coredevAddress,
      gas: '2000000'
    });

    [campaignAddress] = await factory.methods.getDeployedCampaigns().call();
    campaign = await new web3.eth.Contract(
      JSON.parse(compiledCampaign.interface),
      campaignAddress
    );
  });

  it('accounts[1] deploys a campaign', async () => {
    assert.ok(campaign.options.address);
  });

  it('accounts[1] nickname is flagged as used', async () => {
    const isUsed = await factory.methods.checkNickname(coreDevNickname).call();
    assert(isUsed);
  });

  it('verify deployed campaigns', async () => {
    // There must be one campaign deployed
    var countCampaigns = await factory.methods.getCampaignsCount().call();
    assert.equal(countCampaigns, 1);
    // Check indexCampaign for first campaign is zero
    var index = await factory.methods.indexCampaign(campaignAddress).call();
    assert.equal(index,0);

    // Let number of campaigns be up to 5
    await factory.methods.setMaxCampaigns(5).send ({
      from: managerAddress,
      gas: '2000000'
    });

    // Lets now test adding 4 more compaigns and check
    for (var i=0; i<4; i++) {
      await factory.methods.createCampaign('100','Summary',coreDevNickname,'URL','10').send ({
        from: coredevAddress,
        gas: '2000000'
      });
    }
    countCampaigns = await factory.methods.getCampaignsCount().call();
    assert.equal(countCampaigns, 5);
    var campaignAdd = await factory.methods.getDeployedCampaigns().call();
    var camp;
    for (var i=1; i<5; i++) {
      camp = await new web3.eth.Contract(
        JSON.parse(compiledCampaign.interface),
        campaignAdd[i]
      );
      assert.ok(camp.options.address);
      // Check indexCampaign for second campaign is one
      var index = await factory.methods.indexCampaign(campaignAdd[i]).call();
      assert.equal(index,i);
    }
  });

  it('verify infoCampaigns struct data is correct', async () => {
    const infoCampaigns = await factory.methods.infoCampaigns(0).call();
    assert.equal(infoCampaigns.campaignOwner,coredevAddress);
    assert.equal(infoCampaigns.summaryInfo,'Summary');
    assert.equal(infoCampaigns.ownerNickname,coreDevNickname);
    assert.equal(infoCampaigns.urlProject,'URL');
    assert(!infoCampaigns.ownerFinish);
    assert(!infoCampaigns.campaignFinalized);
  });

  it('verify infoCoreDev struct data is correct', async () => {
    const infoCoreDevs = await factory.methods.infoCoreDevs(1).call();
    assert.equal(infoCoreDevs.nickName,coreDevNickname);
    assert.equal(infoCoreDevs.addressCoreDev,coredevAddress);
    assert(infoCoreDevs.enableCoreDev);
  });

  it('verify there are only 2 Core Developers => Manager and coredevAddress', async () => {
    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(coredevAddress).call();
    const countCoreDevs = contractData[1];
    const indexCoreDevs = contractData[3];
    const isCoreDev = devData[0];
    assert.equal(countCoreDevs, '2');
    assert.equal(indexCoreDevs, '2');
    assert(isCoreDev);
  });

  it('verify there are only 1 Community Developer = Manager and coredevAddress', async () => {
    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(coredevAddress).call();
    const countCommDevs = contractData[2];
    const indexCommDevs = contractData[4];
    const isCommDev = devData[1];
    assert.equal(countCommDevs, '1'); // Only 1 here as it has not been approved/
    assert.equal(indexCommDevs, '1');
    assert(!isCommDev); // isCommDev = false as it has not been approved.
  });

  it('verify coredevAddress cannot be registered again as Core developer', async () => {
    try {
      await factory.methods.registerDev('CoreDev2', true).send ({
        from: coredevAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    const contractData = await factory.methods.getContractData().call();
    const countCoreDevs = contractData[1];
    const indexCoreDevs = contractData[3];
    assert.equal(countCoreDevs, '2');
    assert.equal(indexCoreDevs, '2');
  });


  it('verify coredevAddress cannot be registered again as Comm developer', async () => {
    try {
      await factory.methods.registerDev('CoreDev2', false).send ({
        from: coredevAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    const contractData = await factory.methods.getContractData().call();
    const countCommDevs = contractData[2];
    const indexCommDevs = contractData[4];
    assert.equal(countCommDevs, '1'); // Only 1 here as it has not been approved/
    assert.equal(indexCommDevs, '2');
  });

  it('verify coredevAddress can approve accounts[2] as new Core developer', async () => {
    // accounts[2] send a request to register as a CoreDev.
    await factory.methods.registerDev('CoreDev2', true).send ({
      from: accounts[2],
      gas: '2000000'
    });
    // coredevAddress approves the new CoreDev registration request
    await factory.methods.approveCoreDev(accounts[2]).send ({
      from: coredevAddress,
      gas: '2000000'
    });
    const contractData = await factory.methods.getContractData().call();
    const countCoreDevs = contractData[1];
    const indexCoreDevs = contractData[3];
    const isCoreDev = await factory.methods.isCoreDev(accounts[2]).call();
    assert.equal(countCoreDevs, '3');
    assert.equal(indexCoreDevs, '3');
    assert(isCoreDev);
    // Verify infoCoreDev data is correct
    const infoCoreDevs = await factory.methods.infoCoreDevs(2).call();
    assert.equal(infoCoreDevs.nickName,'CoreDev2');
    assert.equal(infoCoreDevs.addressCoreDev,accounts[2]);
    assert(infoCoreDevs.enableCoreDev);
  });

  it('verify coredevAddress can approve accounts[2] as new Comm developer', async () => {
    // accounts[2] send a request to register as a CommDev.
    await factory.methods.registerDev('CommDev2', false).send ({
      from: accounts[2],
      gas: '2000000'
    });
    // Manager approves the new CommDev registration request
    await factory.methods.approveCommDev(accounts[2]).send ({
      from: coredevAddress,
      gas: '2000000'
    });
    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(accounts[2]).call();
    const countCommDevs = contractData[2];
    const indexCommDevs = contractData[4];
    const isCommDev = devData[1];
    assert.equal(countCommDevs, '2');
    assert.equal(indexCommDevs, '2');
    assert(isCommDev);
    // Verify infoCommDev data is correct
    const infoCommDevs = await factory.methods.infoCommDevs(1).call();
    assert.equal(infoCommDevs.nickName,'CommDev2');
    assert.equal(infoCommDevs.addressCommDev,accounts[2]);
    assert(infoCommDevs.enableCommDev);
  });

  it('verify coredevAddress cannot delete itself from Core developer', async () => {
    try {
      await factory.methods.deleteCoreDev(coredevAddress).send ({
        from: coredevAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      // Execution of deleteCoreDev() should fail. This is the expected result
      assert(err);
    }

    const contractData = await factory.methods.getContractData().call();
    const devData = await factory.methods.getDevData(coredevAddress).call();
    const countCoreDevs = contractData[1];
    const indexCoreDevs = contractData[3];
    const isCoreDev = devData[0];
    const isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '2');
    assert.equal(indexCoreDevs, '2');
    assert(isCoreDev);
    assert(!isCoreDevDeleted);
    // Verify infoCoreDev data is still correct
    const infoCoreDevs = await factory.methods.infoCoreDevs(1).call();
    assert.equal(infoCoreDevs.nickName,coreDevNickname);
    assert.equal(infoCoreDevs.addressCoreDev,coredevAddress);
    assert(infoCoreDevs.enableCoreDev);
  });

  it('verify coredevAddress cannot delete another Core developer', async () => {
    // Lets register a new Core developer
    await factory.methods.registerDev('CoreDev2', true).send ({
      from: accounts[2],
      gas: '2000000'
    });
    await factory.methods.approveCoreDev(accounts[2]).send ({
      from: coredevAddress,
      gas: '2000000'
    });
    var contractData = await factory.methods.getContractData().call();
    var devData = await factory.methods.getDevData(accounts[2]).call();
    var countCoreDevs = contractData[1];
    var indexCoreDevs = contractData[3];
    var isCoreDev = devData[0];
    var isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '3');
    assert.equal(indexCoreDevs, '3');
    assert(isCoreDev);
    assert(!isCoreDevDeleted);
    try {
      await factory.methods.deleteCoreDev(accounts[2]).send ({
        from: coredevAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[2]).call();
    countCoreDevs = contractData[1];
    indexCoreDevs = contractData[3];
    isCoreDev = devData[0];
    isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '3');
    assert.equal(indexCoreDevs, '3');
    assert(isCoreDev);
    assert(!isCoreDevDeleted);
    // Verify infoCoreDev data is still correct and updated
    const infoCoreDevs = await factory.methods.infoCoreDevs(2).call();
    assert.equal(infoCoreDevs.nickName,'CoreDev2');
    assert.equal(infoCoreDevs.addressCoreDev,accounts[2]);
    assert(infoCoreDevs.enableCoreDev);
  });

  it('verify coredevAddress can approve back a deleted Core developer', async () => {
    // Lets register a new Core developer
    await factory.methods.registerDev('CoreDev2', true).send ({
      from: accounts[2],
      gas: '2000000'
    });
    await factory.methods.approveCoreDev(accounts[2]).send ({
      from: coredevAddress,
      gas: '2000000'
    });
    var contractData = await factory.methods.getContractData().call();
    var devData = await factory.methods.getDevData(accounts[2]).call();
    var countCoreDevs = contractData[1];
    var indexCoreDevs = contractData[3];
    var isCoreDev = devData[0];
    var isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '3');
    assert.equal(indexCoreDevs, '3');
    assert(isCoreDev);
    assert(!isCoreDevDeleted);
    // Manager delets the account as it is the only authorized
    await factory.methods.deleteCoreDev(accounts[2]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[2]).call();
    countCoreDevs = contractData[1];
    indexCoreDevs = contractData[3];
    isCoreDev = devData[0];
    isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '2');
    assert.equal(indexCoreDevs, '3');
    assert(!isCoreDev);
    assert(isCoreDevDeleted);
    // accounts[2] sends a new registration request as Core developer
    await factory.methods.registerDev('CoreDev2', true).send ({
      from: accounts[2],
      gas: '2000000'
    });
    await factory.methods.approveCoreDev(accounts[2]).send ({
      from: coredevAddress,
      gas: '2000000'
    });
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[2]).call();
    countCoreDevs = contractData[1];
    indexCoreDevs = contractData[3];
    isCoreDev = devData[0];
    isCoreDevDeleted = devData[2];
    assert.equal(countCoreDevs, '3');
    assert.equal(indexCoreDevs, '3');
    assert(isCoreDev);
    assert(!isCoreDevDeleted);
    // Verify infoCoreDev data is correct
    const infoCoreDevs = await factory.methods.infoCoreDevs(2).call();
    assert.equal(infoCoreDevs.nickName,'CoreDev2');
    assert.equal(infoCoreDevs.addressCoreDev,accounts[2]);
    assert(infoCoreDevs.enableCoreDev);
  });

  it('verify coredevAddress cannot delete another Comm developer', async () => {
    // Lets register a new Core developer
    await factory.methods.registerDev('CommDev2', false).send ({
      from: accounts[2],
      gas: '2000000'
    });
    await factory.methods.approveCommDev(accounts[2]).send ({
      from: coredevAddress,
      gas: '2000000'
    });
    var contractData = await factory.methods.getContractData().call();
    var devData = await factory.methods.getDevData(accounts[2]).call();
    var countCommDevs = contractData[2];
    var indexCommDevs = contractData[4];
    var isCommDev = devData[1];
    var isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '2'); // Only 2 ass accounts[2] has not been authorized
    assert.equal(indexCommDevs, '2');
    assert(isCommDev);
    assert(!isCommDevDeleted);
    try {
      await factory.methods.deleteCommDev(accounts[2]).send ({
        from: coredevAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[2]).call();
    countCommDevs = contractData[2];
    indexCommDevs = contractData[4];
    isCommDev = devData[1];
    isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '2');
    assert.equal(indexCommDevs, '2');
    assert(isCommDev);
    assert(!isCommDevDeleted);
    // Verify infoCommDev data is correct
    const infoCommDevs = await factory.methods.infoCommDevs(1).call();
    assert.equal(infoCommDevs.nickName,'CommDev2');
    assert.equal(infoCommDevs.addressCommDev,accounts[2]);
    assert(infoCommDevs.enableCommDev);
  });

  it('verify coredevAddress can approve back a deleted Comm developer', async () => {
    // Lets register a new Comm developer
    await factory.methods.registerDev('CommDev2', false).send ({
      from: accounts[2],
      gas: '2000000'
    });
    await factory.methods.approveCommDev(accounts[2]).send ({
      from: coredevAddress,
      gas: '2000000'
    });
    var contractData = await factory.methods.getContractData().call();
    var devData = await factory.methods.getDevData(accounts[2]).call();
    var countCommDevs = contractData[2];
    var indexCommDevs = contractData[4];
    var isCommDev = devData[1];
    var isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '2');
    assert.equal(indexCommDevs, '2');
    assert(isCommDev);
    assert(!isCommDevDeleted);
    // Manager deletes the CommDev account
    await factory.methods.deleteCommDev(accounts[2]).send ({
      from: managerAddress,
      gas: '2000000'
    });
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[2]).call();
    countCommDevs = contractData[2];
    indexCommDevs = contractData[4];
    isCommDev = devData[1];
    isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '1');
    assert.equal(indexCommDevs, '2');
    assert(!isCommDev);
    assert(isCommDevDeleted);
    // accounts[2] sends a nre registration request as Comm developer
    await factory.methods.registerDev('CommDev2', false).send ({
      from: accounts[2],
      gas: '2000000'
    });
    await factory.methods.approveCommDev(accounts[2]).send ({
      from: coredevAddress,
      gas: '2000000'
    });
    contractData = await factory.methods.getContractData().call();
    devData = await factory.methods.getDevData(accounts[2]).call();
    countCommDevs = contractData[2];
    indexCommDevs = contractData[4];
    isCommDev = devData[1];
    isCommDevDeleted = devData[3];
    assert.equal(countCommDevs, '2');
    assert.equal(indexCommDevs, '2');
    assert(isCommDev);
    assert(!isCommDevDeleted);
    // Verify infoCommDev data is correct
    const infoCommDevs = await factory.methods.infoCommDevs(1).call();
    assert.equal(infoCommDevs.nickName,'CommDev2');
    assert.equal(infoCommDevs.addressCommDev,accounts[2]);
    assert(infoCommDevs.enableCommDev);
  });
});

describe('##### Test Campaign Contract ##### - Tests using CoreDev accounts[1]', () => {
  beforeEach(async () => {
    accounts = await web3.eth.getAccounts();
    managerAddress = accounts[0];
    coredevAddress = accounts[1];

    // managerAddress is the Factory owner
    factory = await new web3.eth.Contract(JSON.parse(compiledFactory.interface))
      .deploy({ data: compiledFactory.bytecode })
      .send({ from: managerAddress, gas: '6000000' });

    // accounts[1] send a request to register as a CoreDev.
    await factory.methods.registerDev(coreDevNickname, true).send ({
      from: coredevAddress,
      gas: '2000000'
    });
    // Manager approves the new CoreDev registration request
    await factory.methods.approveCoreDev(coredevAddress).send ({
      from: managerAddress,
      gas: '2000000'
    });

    // accounts[1] (CoreDev) creates a new campaign
    await factory.methods.createCampaign(minimumContribution,'Summary',coreDevNickname,'URL','10').send ({
      from: coredevAddress,
      gas: '2000000'
    });

    [campaignAddress] = await factory.methods.getDeployedCampaigns().call();
    campaign = await new web3.eth.Contract(
      JSON.parse(compiledCampaign.interface),
      campaignAddress
    );
  });

  it('New Campaign instance, in this test the campaign manager is coredevAddress', async () => {
    const manager = await campaign.methods.manager().call();
    assert.equal(coredevAddress, manager);
  });

  it('New Campaign instance minimumContribution is correct', async () => {
    const minimum = await campaign.methods.minimumContribution().call();
    assert.equal(minimum, minimumContribution);
  });

  it('New Campaign instance coredevAddress is not in the list of approvers', async () => {
    var isApprover = await campaign.methods.approvers(managerAddress).call();
    assert(isApprover);
    isApprover = await campaign.methods.approvers(coredevAddress).call();
    assert(!isApprover);
  });

  it('New Campaign instance approversCount is initially 1', async () => {
    const approversCount = await campaign.methods.approversCount().call();
    assert.equal(approversCount,1);
  });

  it('New Campaign instance supportersCount is initially 0', async () => {
    const supportersCount = await campaign.methods.supportersCount().call();
    assert.equal(supportersCount,0);
  });

  it('New Campaign instance does not have any withdrawals requests', async () => {
    const requestsCount = await campaign.methods.getRequestsCount().call();
    assert.equal(requestsCount,0);
  });

  it('CoreDev can register as a campaign approver', async () =>{
    // coredevAddress registers as a Campaign approver
    await campaign.methods.campaignApprover().send({
      from: coredevAddress,
      gas: '2000000'
    });
    isApprover = await campaign.methods.approvers(coredevAddress).call();
    assert(isApprover);
  });

  it('other Address cannot register as a campaign approver', async () =>{
    const otherAddress = accounts[5];
    try {
      // otherAddress tries registers as a Campaign approver
      await campaign.methods.campaignApprover().send({
        from: otherAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
    isApprover = await campaign.methods.approvers(otherAddress).call();
    assert(!isApprover);
  });

  it('coredevAddress can contribute funds to Campaign - related Campaign state parameters are updated', async () => {
    // Contribute 200 Wei
    const contribution = '200';
    await campaign.methods.contribute().send({
      value: contribution,
      from: coredevAddress,
      gas: '2000000'
    });
    // Supporters count is now incremented to 1
    const supportersCount = await campaign.methods.supportersCount().call();
    assert.equal(supportersCount,1);
    // We keep track of funds provided by managerAddress
    const amount = await campaign.methods.fundsRcvd(coredevAddress).call();
    assert.equal(amount, contribution);
    // Campaign balance equal to contribution
    const summary = await campaign.methods.getSummary().call();
    assert.equal(summary[1], contribution);
  });

  it('Other address can contribute funds to Campaign - related Campaign state parameters are updated', async () => {
    const otherAddress = accounts[5];
    // Contribute 200 Wei
    const contribution = '200';
    await campaign.methods.contribute().send({
      value: contribution,
      from: otherAddress,
      gas: '2000000'
    });
    // Supporters count is now incremented to 1
    const supportersCount = await campaign.methods.supportersCount().call();
    assert.equal(supportersCount,1);
    // We keep track of funds provided by managerAddress
    const amount = await campaign.methods.fundsRcvd(otherAddress).call();
    assert.equal(amount, contribution);
    // Campaign balance equal to contribution
    const summary = await campaign.methods.getSummary().call();
    assert.equal(summary[1], contribution);
  });

  it('coredevAddress cannot contribut less than minimum contribution', async () => {
    const lessContrib = minimumContribution.valueOf() - 1;
    try {
      await campaign.methods.contribute().send({
        value: lessContrib,
        from: coredevAddress,
        gas: '2000000'
      });
      assert(false);
    } catch(err) {
      // function call must fail, this is the expected behavior.
      assert(err);
    }
  });

  it('Other address cannot contribut less than minimum contribution', async () => {
    const otherAddress = accounts[5];
    const lessContrib = minimumContribution.valueOf() - 1;
    try {
      await campaign.methods.contribute().send({
        value: lessContrib,
        from: otherAddress,
        gas: '2000000'
      });
      assert(false);
    } catch(err) {
      // function call must fail, this is the expected behavior.
      assert(err);
    }
  });

  it('coredevAddress cannot create a withdrawal request if Campaign hasn\'t enough funds', async () => {
    // Campaign will have 500 Wei
    const contribute = '500';
    const anyAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: anyAddress,
      gas: '2000000'
    });
    // Manager creates a withdrawal request gratter than current Campaign balance
    const amount = contribute.valueOf() + 1;
    try {
      await campaign.methods
        .createRequest('Buy batteries', amount, coredevAddress)
        .send({
          from: coredevAddress,
          gas: '2000000'
        });
      assert(false);
    } catch (err) {
      assert(err);
    }
    // Number of withdrawal request must be 0
    const requestsCount = await campaign.methods.getRequestsCount().call();
    assert.equal(requestsCount,0);
  });

  it('coredevAddress can create a withdrawal request if Campaign has enough funds', async () => {
    // Campaign will have 500 Wei
    const contribute = '500';
    const anyAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: anyAddress,
      gas: '2000000'
    });
    // Manager creates a withdrawal request
    const amount = contribute.valueOf();
    await campaign.methods
      .createRequest('Buy batteries', amount, coredevAddress)
      .send({
        from: coredevAddress,
        gas: '2000000'
      });
    // Number or withdrawal requests must be 1
    const requestsCount = await campaign.methods.getRequestsCount().call();
    assert.equal(requestsCount, 1);
    // Lets check request data
    const request = await campaign.methods.requests(0).call();
    assert.equal('Buy batteries', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(coredevAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '0');
  });

  it('other Address cannot create a withdrawal request', async () => {
    // Campaign will have 500 Wei
    const contribute = '500';
    const otherAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: otherAddress,
      gas: '2000000'
    });
    try {
      // otherAddress tries to create a withdrawal request
      const amount = contribute.valueOf() - 1;
      await campaign.methods
        .createRequest('Buy batteries', amount, otherAddress)
        .send({
          from: otherAddress,
          gas: '2000000'
        });
      assert(false);
    } catch (err) {
      assert(err);
    }
    // Number of withdrawal request must be 0
    const requestsCount = await campaign.methods.getRequestsCount().call();
    assert.equal(requestsCount,0);
  });

  it('coredevAddress and Manager approve a withdrawal request', async () => {
    // Campaign will have 10 Ether
    const contribute = web3.utils.toWei('10', 'ether');
    const otherAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: otherAddress,
      gas: '2000000'
    });

    let prevBalance = await web3.eth.getBalance(coredevAddress);
    balance = web3.utils.fromWei(prevBalance, 'ether');
    prevBalance = parseFloat(balance);

    // Campaign owner creates a withdrawal request
    const amount = web3.utils.toWei('5', 'ether');
    await campaign.methods
      .createRequest('A', amount, coredevAddress)
      .send({
        from: coredevAddress,
        gas: '2000000'
      });
    // Lets check request data
    var request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(coredevAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '0');

    // coredevAddress registers as a Campaign approver
    await campaign.methods.campaignApprover().send({
      from: coredevAddress,
      gas: '2000000'
    });
    const approver = await campaign.methods.approvers(coredevAddress).call();
    assert(approver);

    // coredevAddress aproves the withdrawal request
    await campaign.methods.approveRequest(0).send({
      from: coredevAddress,
      gas: '2000000'
    });
    // managerAddress aproves the withdrawal request
    await campaign.methods.approveRequest(0).send({
      from: managerAddress,
      gas: '2000000'
    });
    // Lets check request data
    request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(coredevAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '2');

    // The request owner finilizes the withdrawal request.
    await campaign.methods.finalizeRequest(0).send({
      from: coredevAddress,
      gas: '2000000'
    });
    // Lets check request data
    request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(coredevAddress, request.recipient);
    assert(request.complete);
    assert.equal(request.approvalCount, '2');

    let newBalance = await web3.eth.getBalance(coredevAddress);
    balance = web3.utils.fromWei(newBalance, 'ether');
    newBalance = parseFloat(balance);
    assert(newBalance > prevBalance);
  });

  it('Approver will fail to approve a second time a withdrawal request.', async () => {
    // Campaign will have 10 Ether
    const contribute = web3.utils.toWei('10', 'ether');
    const otherAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: otherAddress,
      gas: '2000000'
    });

    // Campaign owner creates a withdrawal request
    const amount = web3.utils.toWei('5', 'ether')
    await campaign.methods
      .createRequest('A', amount, coredevAddress)
      .send({
        from: coredevAddress,
        gas: '2000000'
      });
    // Lets check request data
    var request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(coredevAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '0');

    // coredevAddress registers as a Campaign approver
    await campaign.methods.campaignApprover().send({
      from: coredevAddress,
      gas: '2000000'
    });
    const approver = await campaign.methods.approvers(coredevAddress).call();
    assert(approver);

    // A campaign approver aproves the withdrawal request the first time
    try {
      await campaign.methods.approveRequest(0).send({
        from: coredevAddress,
        gas: '2000000'
      });
    } catch (err) {
      assert(false);
    }
    // Lets check request data
    request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(coredevAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '1');

    // The same approver fails to approve the withdrawal request the second time
    try {
      await campaign.methods.approveRequest(0).send({
        from: coredevAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });

  it('other Address cannot approve a withdrawal request', async () => {
    // Campaign will have 10 Ether
    const contribute = web3.utils.toWei('10', 'ether');
    const otherAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: otherAddress,
      gas: '2000000'
    });

    // Campaign owner creates a withdrawal request
    const amount = web3.utils.toWei('5', 'ether');
    await campaign.methods
      .createRequest('A', amount, coredevAddress)
      .send({
        from: coredevAddress,
        gas: '2000000'
      });
    // Lets check request data
    var request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(coredevAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '0');

    // Other address tries to aprove the withdrawal request
    try {
      await campaign.methods.approveRequest(0).send({
        from: otherAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      // It should fail, so test can pass
      assert(err);
    }
    // Lets check request data
    request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(coredevAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '0');
  });

  it('otherAddress fails to finalize a withdrawal request', async () => {
    // Campaign will have 10 Ether
    const contribute = web3.utils.toWei('10', 'ether');
    const otherAddress = accounts[5];
    await campaign.methods.contribute().send({
      value: contribute,
      from: otherAddress,
      gas: '2000000'
    });

    // Campaign owner creates a withdrawal request
    const amount = web3.utils.toWei('5', 'ether');
    await campaign.methods
      .createRequest('A', amount, coredevAddress)
      .send({
        from: coredevAddress,
        gas: '2000000'
      });
    // Lets check request data
    var request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(coredevAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '0');

    // coredevAddress registers as a Campaign approver
    await campaign.methods.campaignApprover().send({
      from: coredevAddress,
      gas: '2000000'
    });
    const approver = await campaign.methods.approvers(coredevAddress).call();
    assert(approver);

    // A campaign approver aproves the withdrawal request
    await campaign.methods.approveRequest(0).send({
      from: coredevAddress,
      gas: '2000000'
    });
    // managerAddress aproves the withdrawal request
    await campaign.methods.approveRequest(0).send({
      from: managerAddress,
      gas: '2000000'
    });
    // Lets check request data
    request = await campaign.methods.requests(0).call();
    assert.equal('A', request.description);
    assert((request.value.valueOf()/amount.valueOf()) <= (1 - lotteryPercentage.valueOf()/100));
    assert.equal(coredevAddress, request.recipient);
    assert(!request.complete);
    assert.equal(request.approvalCount, '2');

    try {
      await campaign.methods.finalizeRequest(0).send({
        from: otherAddress,
        gas: '2000000'
      });
      assert(false);
    } catch (err) {
      assert(err);
    }
  });
});
