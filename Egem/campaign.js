/******************************************************************************/
//
//  EGEM Crowdfunding - Egem/campaigns.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import web3 from './web3';
import Campaign from './build/Campaign.json';

export default (address) => {
  return new web3.eth.Contract(
    JSON.parse(Campaign.interface),
    address
  );
}
