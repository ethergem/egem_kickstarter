/******************************************************************************/
//
//  EGEM Kickstarter Contract - Egem/Campaign.sol
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
pragma solidity ^0.4.17;

contract CampaignFactory {

    struct InfoCampaign {
      address campaignOwner;
      string summaryInfo;
      string ownerNickname;
      string urlProject;
      bool isFinalized;
    }

    struct InfoCoreDev {
      string nickName;
      address addressCoreDev;
      bool enableCoreDev;
    }

    struct InfoCommDev {
      string nickName;
      address addressCommDev;
      bool enableCommDev;
    }

    address public manager;
    address public contractAddress;
    bool private contractEnable;
    address[] public deployedCampaigns;
    InfoCampaign[] public infoCampaigns;
    InfoCoreDev[] public infoCoreDevs;
    InfoCommDev[] public infoCommDevs;
    uint private countCoreDevs;
    uint private countCommDevs;
    uint private maxCampaigns;
    mapping(address => bool) public isCoreDev;
    mapping(address => bool) private isCommDev;
    mapping(address => bool) private isCoreDevDeleted;
    mapping(address => bool) private isCommDevDeleted;
    mapping(address => uint) private indexCoreDev;
    mapping(address => uint) private indexCommDev;
    mapping(address => uint) private numCampaigns;
    mapping(address => uint) public indexCampaign;
    mapping(address => string) public devNickname;
    mapping(string => bool) usedNickname;
    mapping(address => bool) public campaignFinalized;
    mapping(address => bool) private votedFinalize;
    mapping(address => uint) private numFinalizeVotes;

    modifier restricted() {
        // Only manager is allowed access to this type of restricted functions
        require(msg.sender == manager);
        _;
    }

    modifier coreDevRestricted() {
        // Only CoreDevs are allowed access to this type of restricted functions
        require(isCoreDev[msg.sender]);
        _;
    }

    modifier enabledFactory() {
      // Function can only be executed if contract is enable
      require(contractEnable);
      _;
    }

    // Contract constructor
    function CampaignFactory() public {
        // Store the contract address
        assembly {
            sstore(contractAddress_slot, address)
        }
        // Contract manager
        manager = msg.sender;

        // Initialize some important parameters
        contractEnable = true;
        countCoreDevs = 0;
        countCommDevs = 0;
        devNickname[msg.sender] = 'Manager';
        maxCampaigns = 2;

        // msg.sender becomes first authorized Core developer
        isCoreDev[msg.sender] = true;
        InfoCoreDev memory infoCoreDevData = InfoCoreDev({
          nickName: 'Manager',
          addressCoreDev: msg.sender,
          enableCoreDev: true
          });
        infoCoreDevs.push(infoCoreDevData);
        countCoreDevs++;

        // msg.sender becomes first authorized Community developer
        isCommDev[msg.sender] = true;
        InfoCommDev memory infoCommDevData = InfoCommDev({
          nickName: 'Manager',
          addressCommDev: msg.sender,
          enableCommDev: true
          });
        infoCommDevs.push(infoCommDevData);
        countCommDevs++;

        usedNickname[devNickname[msg.sender]] = true;
    }

    function createCampaign(uint _minimum, string _summary, string _nickname, string _url, uint _lottery) public enabledFactory {
        // Only manager, core devs or community devs can create a new Campaigns.
        require(msg.sender == manager || isCoreDev[msg.sender] || isCommDev[msg.sender]);
        // Will not be able to create more than maxCampaigns active Campaigns
        require(numCampaigns[msg.sender] < maxCampaigns);
        // Lottery pool value must be a valid unit between 0 and 100.
        require(_lottery >= 0 && _lottery <= 100);

        // Deploy a new instance of the Campaign contract.
        // The address of the new deployed Campaign is stored in
        // the newCampaign variable
        address newCampaign = new Campaign(contractAddress, manager, _minimum, _lottery, msg.sender);

        // Store the address of the new deployed Campaign.
        // This address is important to be able to retrieve all Campaign
        // related information.
        deployedCampaigns.push(newCampaign);

        // Store additional information related to the Campaign
        InfoCampaign memory infoData = InfoCampaign({
          campaignOwner: msg.sender,
          summaryInfo: _summary,
          ownerNickname: _nickname,
          urlProject: _url,
          isFinalized: false
          });

        // Store the index of this new campaign to be able to make proper
        // reference to infoCampaigns structure.
        indexCampaign[newCampaign] = infoCampaigns.length;
        infoCampaigns.push(infoData);
        numCampaigns[msg.sender]++;
    }

    function finalizeCampaign(address _campaignAddress) public enabledFactory {
      uint index = indexCampaign[_campaignAddress];
      require(msg.sender == infoCampaigns[index].campaignOwner);
      // The Campaign must have no available balance to Finalize
      require(deployedCampaigns[index].balance == 0);
      campaignFinalized[_campaignAddress] = true;
      infoCampaigns[index].isFinalized = true;
      numCampaigns[msg.sender]--;
    }

    function voteFinalizeCampaign(address _campaignAddress) public coreDevRestricted {
      Campaign _campaign = Campaign(_campaignAddress);
      // The Campaign must have no available balance to Finalize
      require(_campaign.balance == 0);
      // Verify Core developer has not voted already and also that is
      // in the list of approvers for this Campaign
      require(!votedFinalize[msg.sender] && _campaign.approvers(msg.sender));
      votedFinalize[msg.sender] = true;
      numFinalizeVotes[_campaignAddress]++;
      if (numFinalizeVotes[_campaignAddress] > (_campaign.approversCount() / 2)) {
        uint index = indexCampaign[_campaignAddress];
        campaignFinalized[_campaignAddress] = true;
        infoCampaigns[index].isFinalized = true;
        numCampaigns[infoCampaigns[index].campaignOwner]--;
      }
    }

    function enableCampaign(address _campaign) public restricted enabledFactory {
      uint index = indexCampaign[_campaign];
      address owner = infoCampaigns[index].campaignOwner;
      campaignFinalized[_campaign] = false;
      infoCampaigns[index].isFinalized = false;
      numCampaigns[owner]++;
    }

    function getDeployedCampaigns() public enabledFactory view returns (address[]) {
        return deployedCampaigns;
    }

    function getCampaignsCount() public enabledFactory view returns (uint) {
      return infoCampaigns.length;
    }

    function registerDev(string _nickname, bool _coreDev) public enabledFactory {
      // manager is registered with contract constructor, should not execute
      // registerDev() to add again the manager account
      require(msg.sender != manager);
      // Lets check if the request is to register a new CoreDev
      if (_coreDev) {
        if (isCoreDevDeleted[msg.sender]) {
          // This address is already stored in the infoCoreDevs structure. No need to register,
          // just switch flag, to not deleted.
          isCoreDevDeleted[msg.sender] = false;
        } else {
          // One more final check. If indexCoreDev[msg.sender] is 0 then this msg.sender
          // is not in the infoCoreDevs structure, so we store the information.
          if (indexCoreDev[msg.sender] == 0) {
            // Get current length of infoCoreDevs and set as index
            uint indexCore = infoCoreDevs.length;
            // Store the index assigned to this new Core developer
            indexCoreDev[msg.sender] = indexCore;

            InfoCoreDev memory infoCoreDevData = InfoCoreDev({
              nickName: _nickname,
              addressCoreDev: msg.sender,
              enableCoreDev: false
              });

            infoCoreDevs.push(infoCoreDevData);
            usedNickname[_nickname] = true;
          }
        }
      } else {
        // We proceed with request to register as CommDev.
        if (isCommDevDeleted[msg.sender]) {
          // This address is already stored in the infoCommDevs structure. No need to register,
          // just switch flag, to not deleted.
          isCommDevDeleted[msg.sender] = false;
        } else {
          // One more final check. If indexCommDev[msg.sender] is 0 then this msg.sender
          // is not in the infoCommDevs structure, so we store the information.
          if (indexCommDev[msg.sender] == 0) {
            // Get current length of infoCommDevs and set as index
            uint indexComm = infoCommDevs.length;
            // Store the index assigned to this new Core developer
            indexCommDev[msg.sender] = indexComm;
            // Register as Community Developer
            InfoCommDev memory infoCommDevData = InfoCommDev({
              nickName: _nickname,
              addressCommDev: msg.sender,
              enableCommDev: false
              });

            infoCommDevs.push(infoCommDevData);
            usedNickname[_nickname] = true;
          }
        }
      }
    }

    function deleteCoreDev (address _address) public restricted enabledFactory {
      // Failsafe: manager cannot be deleted.
      require(_address != manager);

      uint _index = indexCoreDev[_address];

      isCoreDev[_address] = false;
      InfoCoreDev storage infoCoreDevData = infoCoreDevs[_index];
      infoCoreDevData.enableCoreDev = false;
      countCoreDevs--;
      // Core dev data is still stored in infoCoreDevs, and corresponding enableCoreDev flag
      // has been set to false in structure.
      // Lets use isCoreDevDeleted mapping to switch flag to true for key _address
      isCoreDevDeleted[_address] = true;
    }

    function deleteCommDev (address _address) public restricted enabledFactory {
      // Failsafe: manager cannot be deleted.
      require(_address != manager);

      uint _index = indexCommDev[_address];

      isCommDev[_address] = false;
      InfoCommDev storage infoCommDevData = infoCommDevs[_index];
      infoCommDevData.enableCommDev = false;
      countCommDevs--;
      // Comm dev data is still stored in infoCommDevs, and corresponding enableCommDev flag
      // has been set to false in structure.
      // Lets use isCoreDevDeleted mapping to switch flag to true for key _address
      isCommDevDeleted[_address] = true;
    }

    function approveCoreDev(address _address) public enabledFactory {
      // Only the manager or a Core developer can approve another Core developer request.
      require(msg.sender == manager || isCoreDev[msg.sender] == true);

      uint _index = indexCoreDev[_address];
      InfoCoreDev storage infoCoreDevData = infoCoreDevs[_index];

      // This address is getting approved. Lets remove any isCommDevDeleted[_address] flag
      isCoreDevDeleted[_address] = false;
      devNickname[_address] = infoCoreDevData.nickName;
      isCoreDev[_address] = true;
      infoCoreDevData.enableCoreDev = true;
      countCoreDevs++;
    }

    function approveCommDev(address _address) public enabledFactory {
      // Only the manager or a Core/Community developer can approve another Core developer request.
      require(msg.sender == manager || isCoreDev[msg.sender] == true || isCommDev[msg.sender] == true);

      uint _index = indexCommDev[_address];
      InfoCommDev storage infoCommDevData = infoCommDevs[_index];

      // This address is getting approved. Lets remove any isCommDevDeleted[_address] flag
      isCommDevDeleted[_address] = false;
      devNickname[_address] = infoCommDevData.nickName;
      isCommDev[_address] = true;
      infoCommDevData.enableCommDev = true;
      countCommDevs++;
    }

    function checkNickname(string _nickname) public enabledFactory view returns (bool) {
      return usedNickname[_nickname];
    }

    function getContractData() public enabledFactory view returns (
      bool,     // [0] contractEnable
      uint,     // [1] countCoreDevs
      uint,     // [2] countCommDevs
      uint,     // [3] infoCoreDevs.length
      uint,     // [4] infoCommDevs.length
      uint      // [5] maxCampaigns
      ) {
        return (
          contractEnable,
          countCoreDevs,
          countCommDevs,
          infoCoreDevs.length,
          infoCommDevs.length,
          maxCampaigns
        );
    }

    function getDevData(address _address) public enabledFactory view returns (
      bool,     // [0] isCoreDev
      bool,     // [1] isCommDev
      bool,     // [2] isCoreDevDeleted
      bool,     // [3] isCommDevDeleted
      uint,     // [4] indexCoreDev
      uint,     // [5] indexCommDev
      uint      // [6] numCampaigns
    ) { return (
        isCoreDev[_address],
        isCommDev[_address],
        isCoreDevDeleted[_address],
        isCommDevDeleted[_address],
        indexCoreDev[_address],
        indexCommDev[_address],
        numCampaigns[_address]
        );
    }

    function getCommDevCount() public enabledFactory view returns (uint) {
        return infoCommDevs.length;
    }

    function setContractStatus(bool _status) public restricted {
        contractEnable = _status;
    }

    function setMaxCampaigns(uint _maxCampaigns) public restricted {
      maxCampaigns = _maxCampaigns;
    }
}

contract Campaign {
  struct Request {
      string description;
      uint value;
      uint valueLottery;
      address recipient;
      bool complete;
      uint approvalCount;
      mapping(address => bool) hasApproved;
      address lotteryWinner;
  }

  CampaignFactory factory;
  address public manager;
  address public contractAddress;
  address[] public lotteryPlayers;
  uint public minimumContribution;
  uint public approversCount;
  uint public supportersCount;
  uint public lotteryPercentage;
  uint public totalFundsRcvd;
  bool public refundedCampaign;
  uint public numRefundVotes;
  uint private fundsToRefund;
  mapping(address => bool) public approvers;
  mapping(address => bool) public inLottery;
  mapping(address => uint) public fundsRcvd;
  Request[] public requests;
  mapping(address => bool) public refundVote;
  mapping(address => bool) public accountRefunded;
  mapping(address => uint) public amountRefund;

  modifier restricted() {
      // Only manager is allowed access to restricted functions
      require(msg.sender == manager);
      _;
  }

  modifier factoryManagerRestricted() {
      // Only manager is allowed access to restricted functions
      require(msg.sender == factory.manager());
      _;
  }

  modifier coreDevRestricted() {
      // Only CoreDevs are allowed access to this type of restricted functions
      require(factory.isCoreDev(msg.sender));
      _;
  }

  modifier notFinalized() {
      // Verify if campaign has not Finalized
      require(!factory.campaignFinalized(contractAddress));
      _;
  }

  modifier notRefunded() {
      // Verify if campaign has not been Refunded
      require(!refundedCampaign);
      _;
  }

  function Campaign(address _addFactory, address _approver, uint _minimum, uint _lottery, address _creator) public {
      // Create a reference to the CampaignFactory contract
      factory = CampaignFactory(_addFactory);
      // When a Campaign instance is created _approver can only be the CampaignFactory manager.
      require(_approver == factory.manager());

      // Store the contract address
      assembly {
          sstore(contractAddress_slot, address)
      }
      // Store some additional information
      manager = _creator;
      minimumContribution = _minimum;
      approversCount = 1;
      supportersCount = 0;
      lotteryPercentage = _lottery;
      totalFundsRcvd = 0;
      refundedCampaign = false;
      // CampaignFactory manager becomes the first approver for the Campaign
      approvers[_approver] = true;
  }

  function refundCampaign() public coreDevRestricted notFinalized notRefunded {
    // Verify Core developer has not voted already and also that is
    // in the list of approvers for this Campaign
    require(!refundVote[msg.sender] && approvers[msg.sender]);
    refundVote[msg.sender] = true;
    numRefundVotes++;
    if (numRefundVotes > (approversCount / 2)) {
      // The mayority of Core devs have voted to refund this Campaign
      refundedCampaign = true;
      fundsToRefund = this.balance;
    }
  }

  function getRefund() public {
    // The campaign must be flaged as refundedCampaign=True and
    // the msg.sender must not have received a refund and
    // msg.sender must have contributed funds to this Campaign
    require(refundedCampaign && !accountRefunded[msg.sender] && (fundsRcvd[msg.sender] > 0));
    uint refundAmount = fundsToRefund * fundsRcvd[msg.sender] / totalFundsRcvd;
    amountRefund[msg.sender] = refundAmount;
    accountRefunded[msg.sender] = true;
    if (refundAmount > this.balance) {
      // Send whatever is left.
      msg.sender.transfer(this.balance);
    } else {
      msg.sender.transfer(refundAmount);
    }
  }

  function campaignApprover() public notFinalized notRefunded {
    // To become a Campaign approver, we must be a Core developer
    require(factory.isCoreDev(msg.sender));
    if (!approvers[msg.sender]) {
      // Only add if msg.sender is not already an approver
      approvers[msg.sender] = true;
      approversCount++;
    }
  }

  function contribute() public notFinalized notRefunded payable {
      // First verify if value of Wei sent is greater than the minimumContribution
      require(msg.value > minimumContribution);

      // If we have not contributed funds before, then we are a new supporter, we will
      // add you to the campaign Lottery here.
      if (fundsRcvd[msg.sender] == 0) {
        // Add to Campaign Lottery Here
        supportersCount++;
      }

      // Lets make use of the Mapping hash table and update the related boolean state
      fundsRcvd[msg.sender] += msg.value;
      // Lets keep track of total funds received.
      totalFundsRcvd += msg.value;

      if (!inLottery[msg.sender]) {
        // This new contributor is not in the list of Lottery players
        inLottery[msg.sender] = true;
        lotteryPlayers.push(msg.sender);
      }
  }

  function createRequest(string _description, uint _value, address _recipient) public notFinalized notRefunded restricted {
      uint _valueLottery = _value * lotteryPercentage / 100;
      uint _valueDev = _value - _valueLottery;
      // _recipient must be msg.sender fo this Kickstarter app
      require(msg.sender == _recipient);
      // _value cannot be greater than current contract balance.
      require((_valueDev + _valueLottery) <= this.balance);
      Request memory newRequest = Request({
          description: _description,
          value: _valueDev,
          valueLottery: _valueLottery,
          recipient: _recipient,
          complete: false,
          approvalCount: 0,
          lotteryWinner: 0
      });

      requests.push(newRequest);
  }

  function approveRequest(uint _index) notRefunded public {
      Request storage request = requests[_index];
      // msg.sender must be in the list of approvers and not habe approved the request before
      require(approvers[msg.sender] && !request.hasApproved[msg.sender]);

      request.hasApproved[msg.sender] = true;
      request.approvalCount++;
  }

  function hasApprovedRequest(uint _index, address _address) public view returns (bool) {
    Request storage request = requests[_index];
    return request.hasApproved[_address];
  }

  function finalizeRequest(uint _index) notRefunded public restricted {
      Request storage request = requests[_index];

      require(request.approvalCount > (approversCount / 2));
      require(!request.complete);

      request.complete = true;
      request.recipient.transfer(request.value);

      uint index = randomLottery() % lotteryPlayers.length;
      request.lotteryWinner = lotteryPlayers[index];
      if (request.valueLottery > this.balance)
      {
        // Probably due to gas expenses the remaining balance is less than the requested valueLottery
        // so lest transfer this.balance.
        lotteryPlayers[index].transfer(this.balance);
      } else {
        // Now execute Lottery draw of amount equal to valueLottery
        lotteryPlayers[index].transfer(request.valueLottery);
      }
  }

  function getSummary() public view returns (
    uint, uint, uint, uint, address, uint
    ) {
      return (
        minimumContribution,
        this.balance,
        requests.length,
        supportersCount,
        manager,
        lotteryPercentage
      );
  }

  function getRequestsCount() public view returns (uint) {
      return requests.length;
  }

  function randomLottery() private view returns (uint) {
      return uint(keccak256(block.difficulty, now, lotteryPlayers));
  }

  function getPlayersLottery() public view returns (address[]) {
      return lotteryPlayers;
  }

  function setRefundState(bool _state) public factoryManagerRestricted {
    refundedCampaign = _state;
  }
}
