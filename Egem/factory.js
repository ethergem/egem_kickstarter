/******************************************************************************/
//
//  EGEM Crowdfunding - Egem/factory.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import web3 from './web3';
import CampaignFactory from './build/CampaignFactory.json';

const instance = new web3.eth.Contract(
  JSON.parse(CampaignFactory.interface),
  '0xdF88f926779c3F96BbfdaCAD4d6d3cD6199B325F'
);

export default instance;
