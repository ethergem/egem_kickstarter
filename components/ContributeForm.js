/******************************************************************************/
//
//  EGEM Crowdfunding - components/ContributeForm.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import React, { Component } from 'react';
import { Form, Input, Label, Message, Button } from 'semantic-ui-react';
import Campaign from '../Egem/campaign';
import web3 from '../Egem/web3';
import { Router } from '../routes';

class ContributeForm extends Component {

  state = {
    value: '',
    errorMessage: '',
    loading: false,
    loadingRefund: false
  };

  onGetRefund = async (event) => {
    event.preventDefault();

    const campaign = Campaign(this.props.address);

    this.setState({ loadingRefund: true, errorMessage: '' });
    try {
      const accounts = await web3.eth.getAccounts();

      await campaign.methods.getRefund().send({
          from: accounts[0]
        });

      // Refresh the page
      Router.replaceRoute(`/campaigns/${this.props.address}`);
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ loadingRefund: false, value: '' });
  }

  onSubmit = async (event) => {
    event.preventDefault();

    const campaign = Campaign(this.props.address);

    this.setState({ loading: true, errorMessage: '' });
    try {
      const accounts = await web3.eth.getAccounts();

      await campaign.methods.contribute().send({
          from: accounts[0],
          value: web3.utils.toWei(this.state.value,'ether')
        });

      // Refresh the page
      Router.replaceRoute(`/campaigns/${this.props.address}`);
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ loading: false, value: '' });
  };

  render() {
    return(
      <div>
      <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
        <Form.Field>
          <Label size={'medium'}>
            Amount to Contribute
          </Label>
          <Input
            value={this.state.value}
            onChange={event => this.setState({ value: event.target.value })}
            label='EGEM'
            labelPosition='right'
          />
        </Form.Field>
        <Message error header="Oops!" content={this.state.errorMessage} />
        <Button disabled={(this.state.loading || this.props.disableButtons)} loading={this.state.loading} primary>
          Contribute!
        </Button>
        {(this.props.campaignState == "Refund" && this.props.isContributor) ?
          <Button
            disabled={this.state.loadingRefund}
            loading={this.state.loadingRefund}
            onClick={this.onGetRefund}
            primary>
            Get Refund!
          </Button>
         : null }

      </Form>
      </div>
    );
  }
}

export default ContributeForm;
