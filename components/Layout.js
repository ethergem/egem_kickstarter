/******************************************************************************/
//
//  EGEM Crowdfunding - components/Layout.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import React from 'react';
import { Container } from 'semantic-ui-react';
import Head from 'next/head';
import Header from './Header';

export default props => {
  return (
    <Container style={{color: 'white'}}>
      <Head>
        <link
          rel="stylesheet"
          href="//cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css"
        />
      </Head>
      <style jsx global>{`
        body {
          background-image: url('/static/Background1.jpg') !important;
          background-size: cover !important;
        }
      `}
      </style>
      <Header />
      {props.children}
    </Container>
  );
};
