/******************************************************************************/
//
//  EGEM Crowdfunding - components/RequestCommDevRow.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import React, { Component } from 'react';
import { Button, Label, Table } from 'semantic-ui-react';
import factory from '../Egem/factory';
import web3 from '../Egem/web3';
import { Router } from '../routes';

class RequestCommDevRow extends Component {

  state = {
    isDeletedAddress: false,
    sentApprove: false,
    sentDelete: false,
    errorMessage: ''
  };

  constructor(props) {
    super(props);
    this.checkAccount();
  }

  async checkAccount() {
    const devData = await factory.methods.getDevData(this.props.infoCommDev.addressCommDev).call();
    const isDeletedAddress = devData[3];

    if (isDeletedAddress) this.setState({ isDeletedAddress: true });
  }

  onApprove = async (event) => {
    event.preventDefault();

    this.setState({ sentApprove: true, errorMessage: '' });
    try {
      const { id, infoCommDev, addressMetamask } = this.props;
      await factory.methods.approveCommDev(infoCommDev.addressCommDev)
        .send({ from: addressMetamask});
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ sentApprove: false, errorMessage: '' });
    // Redirect user to the Campaign index page
    Router.pushRoute('/');
  };

  onDelete = async (event) => {
    event.preventDefault();

    this.setState({ sentDelete: true, errorMessage: '' });
    try {
      const { id, infoCommDev, addressMetamask } = this.props;
      await factory.methods.deleteCommDev(infoCommDev.addressCommDev)
        .send({ from: addressMetamask});
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ sentDelete: false, errorMessage: '' });
    // Redirect user to the Campaign index page
    Router.pushRoute('/');
  }

  render() {
    const { Row, Cell } = Table;
    const { id, infoCommDev, isCommDev, isCoreDev, isManager, managerAddress } = this.props;

    return (
      <Row>
        <Cell textAlign='center'>{infoCommDev.nickName}</Cell>
        <Cell textAlign='center'>
          {infoCommDev.addressCommDev}
          {this.state.isDeletedAddress ? ' (DELETED)': null}
        </Cell>
        <Cell textAlign='center'>
          {infoCommDev.enableCommDev ? <Label size='large'>ACTIVE</Label>:
            (isCommDev || isCoreDev) ?
            <a>
            <Button
              color="green"
              disabled={this.state.sentApprove}
              loading={this.state.sentApprove}
              onClick={this.onApprove}
              size='medium'>
              APPROVE
            </Button>
            </a> : 'PENDING'
          }
          {isManager && (infoCommDev.addressCommDev != managerAddress) ?
            <a>
            <Button
              color="red"
              disabled={this.state.sentDelete}
              loading={this.state.sentDelete}
              onClick={this.onDelete}
              size='medium'>
              DELETE
            </Button>
            </a> : null
          }
        </Cell>
      </Row>
    );
  }
}

export default RequestCommDevRow;
