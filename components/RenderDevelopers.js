/******************************************************************************/
//
//  EGEM Crowdfunding - components/RenderDevelopers.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import React, { Component } from 'react';
import { Container, Divider, Header, Label, Table } from 'semantic-ui-react';
import RequestCoreDevRow from '../components/RequestCoreDevRow';
import RequestCommDevRow from '../components/RequestCommDevRow';
import factory from '../Egem/factory';

class RenderDevelopers extends Component {

  renderCoreDevRows() {
    return this.props.infoCoreDevs.map((infoCoreDev, index) => {
      return <RequestCoreDevRow
        key={index}
        id={index}
        infoCoreDev={infoCoreDev}
        isCoreDev={this.props.isCoreDev}
        isManager={this.props.isManager}
        managerAddress={this.props.managerAddress}
        addressMetamask={this.props.addressMetamask}
      />;
    });
  }

  renderCommDevRows() {
    return this.props.infoCommDevs.map((infoCommDev, index) => {
      return <RequestCommDevRow
        key={index}
        id={index}
        infoCommDev={infoCommDev}
        isCommDev={this.props.isCommDev}
        isCoreDev={this.props.isCoreDev}
        isManager={this.props.isManager}
        managerAddress={this.props.managerAddress}
        addressMetamask={this.props.addressMetamask}
      />;
    });
  }

  render() {
    return (
      <Container>
        <Divider />
        <Header
          as='h2'
          style={{color: 'white', marginTop: '20px', marginBottom: '20px'}}
          textAlign='center'>
          {`${this.props.showTitle}`}
        </Header>
        <Table celled style={{ color: 'black', marginTop: 10, marginBottom: 10 }}>
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell textAlign='center' width={2}>
                <Label>Nickname</Label>
              </Table.HeaderCell>
              <Table.HeaderCell textAlign='center' width={10}>
                <Label>Address</Label>
              </Table.HeaderCell>
              <Table.HeaderCell textAlign='center' width={4}>
                <Label>Status</Label>
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>

          <Table.Body>
            { this.props.showCoreDevs ? this.renderCoreDevRows() : this.renderCommDevRows()}
          </Table.Body>
        </Table>
      </Container>
    );
  }
}

export default RenderDevelopers;
