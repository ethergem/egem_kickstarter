/******************************************************************************/
//
//  EGEM Crowdfunding - components/RequestRow.js
//  Currently licensed under MIT
//  A copy of this license must be included in all copies
//  Copyright (c) 2019 Luis Villasenor aka luisvi70
//
/******************************************************************************/
import React, { Component } from 'react';
import { Table, Button } from 'semantic-ui-react';
import web3 from '../Egem/web3';
import Campaign from '../Egem/campaign';
import { Router } from '../routes';

class RequestRow extends Component {

  state = {
    errorMessage: '',
    loadingApprove: false,
    loadingFinalize: false
  };

  onApprove = async () => {
    const campaign = Campaign(this.props.address);
    const accounts = await web3.eth.getAccounts();

    this.setState({ loadingApprove: true });
    try {
      await campaign.methods.approveRequest(this.props.id)
        .send({ from: accounts[0]});
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ loadingApprove: false });
    Router.pushRoute(`/campaigns/${this.props.address}/requests`);
  };

  onFinalize = async () => {
    const campaign = Campaign(this.props.address);
    const accounts = await web3.eth.getAccounts();

    this.setState({ loadingFinalize: true });
    try {
      await campaign.methods.finalizeRequest(this.props.id)
        .send({ from: accounts[0]});
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }
    this.setState({ loadingFinalize: false });
    Router.pushRoute(`/campaigns/${this.props.address}/requests`);
  }

  render() {
    const { Row, Cell } = Table;
    const { id, request, approversCount, isApprover, isCoreDev, isCampaignOwner, hasApproved } = this.props;
    const readyToFinalize = request.approvalCount > approversCount / 2;

    return (
      <Row disabled={request.complete} positive={readyToFinalize && !request.complete}>
        <Cell>{id}</Cell>
        <Cell>{request.description}</Cell>
        <Cell>{web3.utils.fromWei(request.value,'ether')}</Cell>
        <Cell>{request.complete ? request.lotteryWinner : 'Not Finalized' }</Cell>
        <Cell>{request.approvalCount}/{approversCount}</Cell>
        <Cell>
          {!request.complete && isCoreDev && isApprover && !hasApproved ?
            <Button
              color="green"
              basic
              loading={this.state.loadingApprove}
              onClick={this.onApprove}>
              Approve
            </Button>
          : null }
        </Cell>
        <Cell>
          {!request.complete && isCampaignOwner ?
            <Button
              disabled={!readyToFinalize}
              color="teal"
              basic
              loading={this.state.loadingFinalize}
              onClick={this.onFinalize}>
              Finalize
            </Button>
          : null }
        </Cell>
      </Row>
    );
  }
}

export default RequestRow;
